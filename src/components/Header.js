import React from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import { withRouter } from 'react-router'
import { AnchorLink } from './'
import './Header.css'
import logo from '../images/logo-long.svg'

const links = [
  {
    url: '/',
    id: 'home',
    title: 'Domů',
  },
  {
    url: '/informace',
    id: 'information',
    title: 'Informace',
  },
  {
    url: '/workshop',
    id: 'workshop',
    title: 'Workshop',
  },
  {
    url: '/clanky',
    id: 'articles',
    title: 'Články & aktuality',
  },
  {
    url: '/o-projektu',
    id: 'about',
    title: 'O projektu',
  },
]

class Header extends React.Component {
  constructor() {
    super()
    this.state = {
      isHide: true,
      scroll: false,
      visible: false,
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', () => this.hideBar())
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', () => this.hideBar())
  }

  hideBar() {
    const scrollTop = window.scrollY || document.documentElement.scrollTop
    if (scrollTop > 0) {
      this.setState({ scroll: true })
    } else {
      this.setState({ scroll: false })
    }

    if ((scrollTop - this.prev) > 20 || scrollTop < 0 || !this.prev) {
      this.setState({ isHide: true })
      this.prev = scrollTop
    } else if ((this.prev - scrollTop) > 20) {
      this.setState({ isHide: false })
      this.prev = scrollTop
    }
  }

  toggleVisible = () => {
    const actualState = this.state.visible
    this.setState({ visible: !actualState })
  }

  render() {
    const { visible } = this.state
    const { location } = this.props
    console.log(location);

    const show = this.state.isHide || visible ? '' : 'show'
    const scrolled = this.state.scroll ? 'scrolled' : ''
    return (
      <header className={`header ${visible && window.innerWidth < 768 ? 'open' : ''} ${scrolled} ${show}`}>
        <div className="container">
          <div className="container__logo">
            <AnchorLink className="logo" onClick={this.toggleVisible} style={{ margin: 0, }} to="/">
              <img src={logo} alt="Kandiduju.cz" />
            </AnchorLink>
          </div>
          <nav className="container__nav">
            {links.map((link, i) => (
              <AnchorLink to={link.url} className={location.pathname === link.url ? 'activelink' : ''} onClick={this.toggleVisible} key={i}>
                {link.title}
              </AnchorLink>
            ))}
            <a
              href="https://www.facebook.com/Kandidujucz-977270829117287/"
              target="_blank"
              className="facebook"
              title="Facebook"
            >
              <svg fill="white" viewBox="0 0 512 512">
                <path d="M211.9 197.4h-36.7v59.9h36.7V433.1h70.5V256.5h49.2l5.2-59.1h-54.4c0 0 0-22.1 0-33.7 0-13.9 2.8-19.5 16.3-19.5 10.9 0 38.2 0 38.2 0V82.9c0 0-40.2 0-48.8 0 -52.5 0-76.1 23.1-76.1 67.3C211.9 188.8 211.9 197.4 211.9 197.4z"/>
              </svg>
            </a>
          </nav>
          <button className="toggle" onClick={this.toggleVisible}>
            <div className="hamburger">
              <span />
              <span />
              <span />
            </div>
          </button>
        </div>
      </header>
    )
  }
}

Header.propTypes = {
  visible: PropTypes.bool.isRequired,
}


export default withRouter(Header)
