import React from 'react'
import { Section } from '../'
import logo from '../../images/logo_transparent_crop.png'
//import './ContactUsSection.scss'
// ha
const AboutSection = () => (
  <Section secondaryTitle="Za projektem stojí" type="grey" >
    <div className="about-section">
      <div className="about-section__logo" style={{ textAlign: 'center' }}>
        <a href="http://mladiobcane.cz/" target="_blank" rel="noopener noreferer">
          <img src={logo} alt="Mladí Občané" style={{ maxWidth: '100%', width: '30rem', margin: '10rem auto' }} />
        </a>
      </div>
    </div>

  </Section>
)

export default (AboutSection)