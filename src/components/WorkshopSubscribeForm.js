import React from 'react'
import { Button } from './'
import './WorkshopSubscribeForm.css'

class WorkshopSubscribeForm extends React.Component {
  constructor() {
    super()
    this.state = {
      name: '',
      email: '',
      gdpr: false,
    }
    this.handleOnChange = this.handleOnChange.bind(this)
  }

  handleOnChange(field, value) {
    if (field === 'name') this.setState({ name: value })
    if (field === 'email') this.setState({ email: value })
  }

  render() {
    const { handleOnChange } = this
    const { name, email, gdpr } = this.state
    return (
      <form className="workshop-subscribe-form">
        <div className="form-item">
          <div className="form-item__label required">
            Váše jméno
          </div>
          <div className="form-item__field">
            <input
              name="name"
              placeholder='Antonín Novák'
              value={name}
              onChange={(e) => handleOnChange('name', e.target.value)}
            />
          </div>
        </div>

        <div className="form-item">
          <div className="form-item__label required">
            Váš email
          </div>
          <div className="form-item__field">
            <input
              name="email"
              placeholder='budouci.zastupitel@vasedomena.cz'
              value={email}
              onChange={(e) => handleOnChange('email', e.target.value)}
            />
          </div>
        </div>

        {/* <div className="form-item">
          <input
            type="checkbox"
            name="gdpr"
            value={gdpr}
            onChange={(e) => handleOnChange('gdpr', e.target.value)}
          />
          <label htmlFor="gdpr">Subscribe to newsletter?</label>
        </div> */}
        <Button text="Chci dostávat novinky" />
      </form>
    )
  }
}

export default (WorkshopSubscribeForm)