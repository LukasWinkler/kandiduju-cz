import favicon from './favicon'

const navColor = '#CBB88A'

export default {
  title: '%s | Kandiduju.cz',
  metas: [
    { charset: 'utf-8' },
    {
      name: 'viewport',
      content: 'width=device-width, initial-scale=1, shrink-to-fit=no',
    },
    {
      'http-equiv': 'x-ua-compatible',
      content: 'ie=edge',
    },
    {
      name: 'theme-color',
      content: navColor,
    },
    {
      name: 'msapplication-navbutton-color',
      content: navColor,
    },
    {
      name: 'apple-mobile-web-app-status-bar-style',
      content: navColor,
    },
    {
      name: 'description',
      content: 'Portál který Vám pomůže s kandidaturou',
    },
    {
      property: 'og:title',
      content: 'Kandiduju.cz',
    },
    {
      property: 'og:type',
      content: 'website',
    },
    {
      property: 'og:url',
      content: 'https://kandiduju.cz/',
    },
    {
      // TODO
      property: 'og:image',
      content: 'https://',
    },
    {
      property: 'og:site_name',
      content: 'ELSA.CZ',
    },
    {
      property: 'og:description',
      content: 'Portál s pracovními nabídkami v právní oblasti',
    },
    {
      property: 'twitter:title',
      content: 'Kandiduju.cz',
    },
    {
      property: 'twitter:url',
      content: 'https://kandiduju.cz/',
    },
    ...favicon.meta,
  ],
  links: favicon.link,
}
