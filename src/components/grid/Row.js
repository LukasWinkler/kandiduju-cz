import React from 'react'
import PropTypes from 'prop-types'

const Row = ({ children, className }) =>
  <div className={`row ${className}`}>{children}</div>

Row.defaultProps = {
  children: null,
  className: '',
}

Row.propTypes = {
  children: PropTypes.any,
  className: PropTypes.string,
}

export default Row
