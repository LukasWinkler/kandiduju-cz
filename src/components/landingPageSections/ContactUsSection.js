import React from 'react'
import { Section, ContactUsForm } from '../'
import './ContactUsSection.css'

const ContactUsSection = () => (
  <Section title="Kontaktuj nás" type="white">
    <div className="contact-us-section" id='contact-us-section'>
      <div className="contact-us-section__info">
        Máte nějaký dotaz? Není Vám něco jasné? Napište nám nebo nám zavolejte. Velmi rádi Vám odpovíme! :)
      </div>
      <div className="contact-us-section__contacts">
        <p>
          kandiduju@mladiobcane.cz
        </p>
        <p>
          +420 722 983 153
        </p>
      </div>
      {/* <ContactUsForm /> */}
    </div>
  </Section>
)

export default (ContactUsSection)