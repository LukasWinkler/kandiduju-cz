import React from 'react'
import Butter from 'buttercms'
import {
 HeaderSection,
 ArticlesSection,
 InformationSection,
 PartnersSection,
 ContactUsSection,
 InterviewsSection,
 AboutSection,
} from '../components/landingPageSections'

import './LandingPage.css'

const butter = Butter('3fc9af040866537457e8318edc3b5eb94df0b88b')

class LandingPage extends React.Component {
  fetchPosts(page) {
    butter.page.retrieve('1').then((resp) => {
      console.log('resp', resp)
      this.setState({
        loaded: true,
        resp: resp.data
      })
    });
  }
  componentDidMount() {
    window.scrollTo(0, 0)
    document.title = "Kandiduju.cz"
    this.fetchPosts(1)
  }
  render() {
    return(
      <div className="page landing-page">
        <HeaderSection />
        <InterviewsSection articles={articles} />
        <InformationSection />
        <ArticlesSection articles={articles} />
        <PartnersSection />
        <ContactUsSection />
        <AboutSection />
      </div>
    )
  }
}


export default (LandingPage)

const articles = {
  'save-the-date' : {
    date: '15. 6. 2018',
    slug: 'save-the-date',
    type: 'article',
    origin: 'vlastni, foto: www.mmkv.cz',
    image: {
      url: 'http://res.cloudinary.com/farlanis/image/upload/v1529067943/savethedate.jpg',
      smallUrl: 'http://res.cloudinary.com/farlanis/image/upload/v1529067943/savethedate.jpg'
    },
    name: 'Save the date!',
    text: 'Jestli existuje důležitější datum než 5. a 6. října, kdy se konají komunální volby, pak je to zcela jistě 31. červenec. Přesně do 16:00 toho dne máte možnost podávat své kandidátní listiny registračnímu úřadu. Pozor, nestačí je pouze odeslat, do čtyř hodin odpoledne tam musejí být fyzicky doručeny. Registračním úřadem je pak obecní úřad, pakliže má vaše obec zřízeny alespoň 2 odbory. V ostatních případech se musíte obrátit na pověřený obecní úřad. Nemějte ale obavy, pokud si nejste jistí, kam kandidátku dodat. Do 12. července zveřejní na své úřední desce pověřený obecní úřad přehled všech registračních úřadů ve vašem územním obvodu. Tak hodně štěstí, pakliže zrovna sestavuje kandidátky! A nezapomeňte – save the date – 31. červenec!',
    description: <div>
        <p>
          Jestli existuje důležitější datum než 5. a 6. října, kdy se konají komunální volby, pak je to zcela jistě <b>31. červenec</b>. Přesně do 16:00 toho dne máte možnost podávat své kandidátní listiny registračnímu úřadu. Pozor, nestačí je pouze odeslat, do čtyř hodin odpoledne tam musejí být fyzicky doručeny. Registračním úřadem je pak obecní úřad, pakliže má vaše obec zřízeny alespoň 2 odbory. V ostatních případech se musíte obrátit na pověřený obecní úřad. Nemějte ale obavy, pokud si nejste jistí, kam kandidátku dodat. Do 12. července zveřejní na své úřední desce pověřený obecní úřad přehled všech registračních úřadů ve vašem územním obvodu. Tak hodně štěstí, pakliže zrovna sestavuje kandidátky! A nezapomeňte – save the date – 31. červenec!
        </p>
      </div>
  },
  'martin-kupka-proc-spolehat-na-to-ze-me-nekdo-vyslechne-kdyz-muzu-svoje-napady-a-postoje-resit-jako-zastupitel': {
    date: '18. 5. 2018',
    slug: 'martin-kupka-proc-spolehat-na-to-ze-me-nekdo-vyslechne-kdyz-muzu-svoje-napady-a-postoje-resit-jako-zastupitel',
    type: 'interview',
    origin: 'vlastní',
    author: 'Šárka Löffelmannová',
    image: {
      url: 'http://res.cloudinary.com/farlanis/image/upload/v1528724215/Kupka1.jpg',
      smallUrl: 'http://res.cloudinary.com/farlanis/image/upload/v1528724215/Kupka1.jpg'
    },
    videoUrl: 'sfMSk11ZU-E',
    name: 'Martin Kupka: „Proč spoléhat na to, že mě někdo ze starších vyslechne, když můžu svoje nápady a postoje řešit přímo jako zastupitel?“',
    text: 'Obec má blízko k lidem, takže může dobře reflektovat jejich momentální potřeby, což by také dělat měla. Jedno z témat, které se dají na komunální úrovni ovlivnit a které je důležité právě pro rovnost žen a mužů, je dostatečný počet míst ve školkách. Což je věc, kterou má v kompetenci obec, ta může dělat demografické prognózy počtu obyvatel a stavět nové školky, tak aby byla ta kapacita naplněná.  Zároveň genderová rovnost proniká do mnoha různých oblastí. Můžeme ji vidět ve veřejném prostoru. Když si představíme, že jsem matka s malým dítětem, která se chce pohodlně pohybovat po městě s kočárkem, potřebuji hustou síť veřejné dopravy, zeleň a parky, kam můžu chodit s dítětem na procházku, kde si může bezpečně hrát. Všechno souvisí se vším a všechna ta témata se dají promítat i na nejnižší úrovni.',
    description: <div>
        <p><b>Od roku 2010 působíte jako starosta v obci Líbeznice. Co Vás vedlo k rozhodnutí jít do toho?</b></p>
        <p>Já jsem opravdu chtěl udělat něco hmatatelného a viditelného pro to, aby se obec dobře rozvíjela a lidem se tam žilo lépe. Být zastupitelem a dokonce starostou je nejlepší způsob, jak tenhle záměr převést do reality.</p>

        <p><b>Podařilo se Vám v obci zlepšit míru spolupráce s mladými lidmi?</b></p>
        <p>My jsme se před časem zprostředkovaně dozvěděli, že si mladí lidé na facebooku stěžovali, že se u nás děje spousta akcí jen pro nejmenší a pro starší. Byla tam najednou skupinka lidí, kteří si mysleli, že Líbeznce nejsou pro ně.</p>
        <p>My jsme je tehdy oslovili prostřednictvím facebooku a pozvali je na společnou debatu. I s architekty jsme seděli a bavili jsme se o tom, co by mohlo být jinak. Dokonce společně s architektem malovali, jak by si představovali jedno takové důležité místo - Areál zdraví. Z jejich odezvy vyplynulo, že by tam chtěli skate park a posilovací prvky, to tam chybělo a na základě této odezvy se tam začal skate park budovat a dnes funguje. Od té doby se snažíme prostřednictvím sociálních sítí s mladými lidmi mluvit, byť je pro nás větší a větší výzva držet tempo.</p>

        <p><b>Když jste s mladými začali navazovat kontakt, zaznamenali jste nějakou odezvu? Třeba větší zájem o ODS, připadá Vám, že je teď ODS vnímána jako víc otevřená mladým lidem?</b></p>
        <p>Chtěl bych, aby to tak bylo. Časem se ukáže, jestli to tak funguje. My jsme i k výběru zhotovitelů skate parku pozvali skejťáky. A to pro ně byla cenná zkušenost. Protože potom někdo přišel s tím, že celá ta soutěž musela být ohnutá. To se jich hrozně dotklo, protože jako komisaře v soutěži je samozřejmě nikdo neovlivňoval. Oni přišli a z čista jasna se od někoho dozvěděli, že jsou zkorumpovaní. To pro ně byla nepříjemná zkušenost, ale dobře, že ji zažili. Vlastně je to vyprovokovalo k dalším aktivitám. Komunální politika a rozhodování pro ně teď má i tento rozměr. Dnes třeba ještě nejsou v zastupitelstvu, ale pořád se bavíme a ten skate park žije.</p>

        <p><b>Začínal jste jako novinář a moderátor, pomohlo Vám to nějak ve vašem politickém životě?</b></p>
        <p>Myslím si, že jedna ze schopností politika má být schopnost dobře formulovat své myšlenky a názory a vysvětlovat lidem, co by chtěl udělat. K tomu je určitě průprava rozhlasového novináře dobrá. Myslím si, že na tom záleží, protože spousta dobrých věcí se nezrealizovala nebo zanikla, protože nebyl nikdo, kdo by je dovedl správně vysvětlit, předat je tak, aby měli odezvu a lidé to špatně nepochopili.</p>

        <p><b>Máte nějaká doporučení pro mladé lidi? Proč vstoupit do politiky?</b></p>
        <p>Když mladí lidé v politice budou, tak můžou v mozaice názorů lidí různých profesí a věku vyjádřit to, co by si přáli oni a co pokládají za důležité. Když se bude například na zastupitelstvu jednat o rozpočtu, tak tam můžou jednoduše přednést, že by se hodila koruna na skate park nebo na jiné hřiště. Jinak se mohou jen spoléhat, že je bude poslouchat někdo starší a jejich názor přednese, ale lepší je přece říct to napřímo a přímo se účastnit rozhodování.</p>

      </div>
  },
  'lucia-zachariasova-ceske-politice-chybi-dve-veci-mladi-lide-a-zeny-a-to-nejlepe-v-kombinaci': {
    date: '18. 4. 2018',
    slug: 'lucia-zachariasova-ceske-politice-chybi-dve-veci-mladi-lide-a-zeny-a-to-nejlepe-v-kombinaci',
    type: 'interview',
    origin: 'vlastní',
    author: 'Šárka Löffelmannová',
    image: {
      url: 'http://res.cloudinary.com/farlanis/image/upload/v1527609810/zachariasova.jpg',
      smallUrl: 'http://res.cloudinary.com/farlanis/image/upload/v1527609810/zachariasova.jpg'
    },
    videoUrl: 'rA2bOZ44YaI',
    name: 'Lucia Zachariášová: „České politice chybí dvě věci, mladí lidé a ženy a to nejlépe v kombinaci“',
    text: 'Obec má blízko k lidem, takže může dobře reflektovat jejich momentální potřeby, což by také dělat měla. Jedno z témat, které se dají na komunální úrovni ovlivnit a které je důležité právě pro rovnost žen a mužů, je dostatečný počet míst ve školkách. Což je věc, kterou má v kompetenci obec, ta může dělat demografické prognózy počtu obyvatel a stavět nové školky, tak aby byla ta kapacita naplněná.  Zároveň genderová rovnost proniká do mnoha různých oblastí. Můžeme ji vidět ve veřejném prostoru. Když si představíme, že jsem matka s malým dítětem, která se chce pohodlně pohybovat po městě s kočárkem, potřebuji hustou síť veřejné dopravy, zeleň a parky, kam můžu chodit s dítětem na procházku, kde si může bezpečně hrát. Všechno souvisí se vším a všechna ta témata se dají promítat i na nejnižší úrovni.',
    description: <div>
        <p><b>Proč jste se rozhodla vstoupit do komunální politiky a proč zrovna za Stranu zelených?</b></p>
        <p>Já začnu odzadu. Nejdřív jsem se rozhodla, že vstoupím do Strany zelených a těch motivací bylo několik. Samozřejmě jsem souzněla s politikou, kterou Strana zelených dělá a prosazuje. Zaměřuji se hlavně na postavení žen a mužů, v širším slova smyslu na lidská práva a ochranu menšin. Strana zelených tato témata vnímá a snaží se hledat způsoby jak s nimi něco dělat, jak postavení znevýhodněných lidí zlepšit.</p>

        <p><b>Jak se dá v komunální politice ovlivňovat genderová nerovnost, lidská práva a podobná témata?</b></p>
        <p>Obec má blízko k lidem, takže může dobře reflektovat jejich momentální potřeby, což by určitě dělat měla. Jedno z témat, která se dají na komunální úrovni ovlivnit a které je důležité právě pro rovnost žen a mužů, je dostatečný počet míst ve školkách. Což je věc, kterou má v kompetenci obec, ta může stavět nové školky, tak aby byla kapacita dostatečná, dělat demografické prognózy počtu dětí.  Zároveň perspektiva postavení žen a mužů proniká do mnoha různých oblastí. Můžeme ji vidět ve veřejném prostoru. Když si představíme matku s malým dítětem, která se chce pohodlně pohybovat po městě s kočárkem, potřebuje hustou síť veřejné dopravy, zeleň a parky, kam může chodit s dítětem na procházku, kde si může bezpečně hrát. Všechno souvisí se vším a všechna ta témata se dají promítat i na obecní úrovni.</p>

        <p><b>Kandidujete na Praze 3 na Žižkově. Jaké jsou největší ekologické problémy Žižkova? Co by se dalo změnit? Na čem pracujete?</b></p>
        <p>Pokud se bavíme o ekologii, tak nejenom pro Žižkov ale v Praze obecně je to téma dopravy. Mám pocit, že Praha je město aut a zatím se nám poměrně málo daří vytvářet sdílený veřejný prostor  tak, aby byl nejenom pro auta, ale úplně stejně i pro lidi na kole nebo pěší.  To je jedno z velkých témat.  Když se podívám přímo na Prahu 3, tak velmi výrazné rozvojové území je Nákladové nádraží  Žižkov.  To je teď v mezifázi, kdy se rozhoduje, co se s ním bude dít.  Je tam kulturní památka samotného Nákladového nádraží. Také je tam brownfield, na kterém bude výstavba a je důležité ovlivnit, jak ta výstavba bude vypadat a co se s tímhle rozvojovým územím stane.</p>

        <p><b>Co zatím považujete za své největší politické úspěchy?</b></p>
        <p>Já jsem teď v obecním zastupitelstvu první volební období a jsme v opozici, což znamená, že toho nemůžeme zase tolik prosadit. To je důležité, protože lidi, když kandidují, tak se musí smířit s tím, že hned třeba nebudou chod radnice přímo ovlivňovat. Na druhou stranu opoziční práce je důležitá v tom, že kontrolujeme to, co vedení města dělá a snažíme se to korigovat, upozorňovat na chyby a problémy. My jsme se mnohokrát snažili zlepšit přístupnost občanů a občanek na jednání zastupitelstva. Aby mohli mluvit v konkrétní pevnou hodinu, aby nemuseli čekat, až se vypovídáme, ale aby věděli, kdy přesně můžou na zastupitelstvo přijít.  Nebo aby tam byla nějaká hlídací služba, když přijdou rodiče s dětmi, pro které může být složitější s dětmi na zastupitelstvo dorazit. Bohužel se nám to zatím nepodařilo, takže to nelze označit za úspěch, ale budeme v tom pokračovat a doufejme, že se to jednou na Praze 3 povede.</p>

        <p><b>Máte nějaký závěrečný vzkaz pro mladé kandidáty a kandidátky?</b></p>
        <p>Chtěla bych jim vzkázat, aby se zajímali o veřejné dění a o to, co se děje v jejich okolí. Pokud je něco šve, nemá smysl složit ruce do klína a říct si, že to stejně nezměním. Podle mého názoru tou jedinou správnou cestou je prostě se zvednout a něco s tím udělat. Politika a veřejná zastupitelská demokracie je tady od toho, aby právě takhle fungovala, proto si myslím, že je důležité dívat se kolem sebe a snažit se věci měnit podle svého nejlepšího vědomí a svědomí.</p>

      </div>
  },
  'jan-farsky-politika-je-svinstvo-a-vstupem-do-politiky-se-neda-nic-zmenit-oboji-je-lez': {
    date: '13. 4. 2018',
    slug: 'jan-farsky-politika-je-svinstvo-a-vstupem-do-politiky-se-neda-nic-zmenit-oboji-je-lez',
    type: 'interview',
    origin: 'vlastní',
    author: 'Šárka Löffelmannová',
    image: {
      url: 'http://res.cloudinary.com/farlanis/image/upload/v1526838865/farsky.jpg',
      smallUrl: 'http://res.cloudinary.com/farlanis/image/upload/v1526838865/farsky.jpg'
    },
    videoUrl: 'rp8GDalwFNs',
    name: 'Jan Farský: „Politika je svinstvo a vstupem do politiky se nedá nic změnit? Obojí je lež.“',
    text: 'Zároveň si musí vždycky hlídat, aby si zachoval čisté svědomí, aby byl o svých krocích sám přesvědčený, protože jenom tak může měnit věci kolem sebe k lepšímu a přesvědčovat ostatní. A musí být připravený takřka na cokoli, protože čím bude viditelnější, tím větší na něj budou tlaky.',
    description: <div>
        <p>
          <b>Vy jste do politiky vstoupil v roce 2002 ve svých dvaceti dvou letech, co vás k tomu vedlo?</b>
        </p>
        <p>Bylo to v Semilech, kde jsem se dostal do zastupitelstva. Vedlo mě k tomu jednoduše to, že jsem se v Semilech narodil, měl jsem je rád a měl jsem pocit, že můžu pomoct jejich zdárnému rozvoji.</p>

        <p>
          <b>Jaké kroky mohou mladí lidé podniknout k tomu, aby mohli v zastupitelstvech začít něco měnit, přece jenom tam budou nově a pravděpodobně nebudou mít u ostatních takovou autoritu. Máte pro ně nějakou radu, jak se s tím popasovat?</b>
        </p>
        <p>Reálně mají ty možnosti asi dvě. Buď se mohou zapojit do uskupení, které v té obci už kandiduje a zkusit prosadit svoje myšlenky v platformě toho uskupení, ať už to bude strana, hnutí, nebo sdružení nezávislých kandidátů. Nebo si můžou sdružení nezávislých kandidátů sestavit sami. Což znamená mít podpisy od sedmi procent voličů v té oblasti, to na jednu stranu vypadá jako hrozně moc, na druhou stranu jestli chcete něco měnit, tak je důležité mít víc než sedmi-procentní podporu, a tohle už je forma kampaně, takže ta práce se sbíráním podpisů za to stojí. Taky jsem tak začínal.</p>
        <p>A pak prosazovat myšlenky. Ať kandiduji kamkoli, tak musím vědět, co lidem nabízím nového. Protože je nesmyslné kandidovat a nežít pocitem, že budu lepší než ti, co to dělají teď. To by mě asi nikdo nevolil, kdybych říkal, že budu o něco horší, nebo bych nepřinesl něco nového.</p>

        <p>
          <b>Vy jste příkladem člověka, který se ze zastupitele dostal až na post starosty. Pamatujete si na svůj první den starosty?</b>
        </p>
        <p>Ano, pamatuji. Zrovna nedávno jsem na něj vzpomínal, protože to bylo pondělí ráno na radnici, kdy za mnou přišli dva pánové, tehdy ještě studenti semilského gymnázia. S jedním z nich se potkávám dodnes. Po čtyřech letech, když jsem se stal poslancem, jsem si vzpomněl, že byl aktivní a věděl jsem, že šel na práva, tak jsem ho zkontaktoval a stal se mým asistentem. Pak pokračoval v různých pozicích ve sněmovně a dodnes jsme v úzkém kontaktu. Tohle si z prvního dne na radnici pomatuju velmi konkrétně.</p>
        <p>Jinak to bylo samozřejmě náročné v tom, že člověk přebírá obrovskou zodpovědnost a já jsem tehdy necítil zodpovědnost jenom za město. Tím jak tam člověk žije celý život a má tam rodinu, tak je to obrovský nápor, aby nezklamal, protože ve chvíli, kdy zklamu já, tak tím poškodím rodinu. Cítil jsem zodpovědnost za to, že je mi sedmadvacet let, že jsem se stal starostou a že když uspěju, tak tím otevřu dveře do politiky dalším mladým lidem. Ale když neuspěji, když udělám nějaké blbosti, tak jim tím ublížím.
          Bylo to hrozně silné a krásné, protože ty změny ve městě přicházely hrozně rychle. Odpor byl samozřejmě velký, ale to je naprosto přirozené. K jakékoli změně je vždycky odpor. S tím musí počítat každý, kdo chce něco měnit. V mém případě tenhle odpor převážila energie, kterou jsem byl připravený vložit do toho, že to chci změnit a zároveň přesvědčení, že je správný otevřít radnici, komunikovat s lidmi a zatahovat je do rozhodování.</p>

        <p>
          <b>Jaký by podle Vás měl být mladý člověk, jaké musí mít vlastnosti, aby mohl uspět a vydržet v politice?</b>
        </p>
        <p>Měl by být rozhodně svobodný. V tom smyslu, že by neměl být kýmkoli ovladatelný. Neměl by mít v historii morální šrámy, které by znamenaly, že ho někdo může vydírat. Protože na lidech se velmi rychle pozná, že nerozhodují sami za sebe. Nemůže čekat vděk. Práce v politice je krásná a zajímavá, člověk může hodně ovlivnit, ale pokud si vytyčí za cíl, že ho mají mít na konci všichni rádi, tak to neskončí dobře.</p>
        <p>Zároveň si musí vždycky hlídat, aby si zachoval čisté svědomí, aby byl o svých krocích sám přesvědčený, protože jenom tak může měnit věci kolem sebe k lepšímu a přesvědčovat ostatní. A musí být připravený takřka na cokoli, protože čím bude viditelnější, tím větší na něj budou tlaky.</p>
        <p>Velmi důležité je silné morální a osobní zázemí. Pro mě byla od začátku naprosto zásadní a nezbytná opora v rodině, zprvu rodičů a sourozenců, později mé vlastní rodiny. A tu kdybych neměl, tak bych politiku dělat nemohl, protože pak člověk začne mít v životě jediné a to politiku, což je podle mě špatně. Člověk by měl vždycky stát na víc nohou.</p>
        <p>Já to u sebe realizuji tak, že mám rodinu, což je číslo jedna. Pak mám skauting, což je druhá noha, na které stojím. A třetí noha je právě práce. A trojnožka je nejstabilnější možný prvek. Každá z těch nohou mě stabilizuje a naplňuje svým způsobem. Z toho čerpám energii k činnosti. To že to samozřejmě mnohdy nefunguje, to že je člověk mnohdy v koncích, naštvaný, pochybuje o tom, jestli to co dělá je smysluplné a správné, je myslím si dobře, protože jakmile přestane pochybovat, tak začne dělat chyby a někdy nenapravitelné. Tohle je něco, co bych každému přál, aby si našel.</p>

      </div>
  },
  'jakub-michalek-kandidatura-by-nemela-predbehnout-plany-a-napady-na-zmenu': {
    date: '13. 4. 2018',
    slug: 'jakub-michalek-kandidatura-by-nemela-predbehnout-plany-a-napady-na-zmenu',
    type: 'interview',
    origin: 'vlastní',
    author: 'Šárka Löffelmannová',
    image: {
      url: 'http://res.cloudinary.com/farlanis/image/upload/v1526215168/michalek.jpg',
      smallUrl: 'http://res.cloudinary.com/farlanis/image/upload/v1526215168/michalek.jpg'
    },
    videoUrl: 'J909JawsibA',
    name: 'Jakub Michálek: “Kandidatura by neměla předběhnout plány a nápady na změnu, ty musí přijít první.”',
    text: 'Na začátku člověk musí zjistit, co lidé chtějí. Člověk je jen prodlouženou rukou společnosti, vykonává to, co lidi chtějí a co v té dané obci potřebují. Pak to musí zjednodušit do srozumitelné a jednoduché zprávy. Například když je na nějaké obci problém, že tam je hodně seniorů a chybí zdravotní služba, tak musí přijít s tím, jak zajistí, aby se tam dostala záchranka. To řešil Petr Třešňák, když kandidoval na starostu Mariánských Lázní a povedlo se mu to.',
    description: <div>
        <p>
          <b>V komunálních volbách v roce 2014 jste vedl v Praze kandidátku. V malých městech a obcích úspěch ve volbách nezávisí tak významně na velikosti a vychytanosti kampaně. V Praze je ta situace pochopitelně jiná. Máte nějakou radu, jak na to?</b>
        </p>
        <p>Na začátku člověk musí zjistit, co lidé chtějí. Člověk je jen prodlouženou rukou společnosti, vykonává to, co lidi chtějí a co v té dané obci potřebují. Pak to musí zjednodušit do srozumitelné a jednoduché zprávy. Například když je na nějaké obci problém, že tam je hodně seniorů a chybí zdravotní služba, tak musí přijít s tím, jak zajistí, aby se tam dostala záchranka. To řešil Petr Třešňák, když kandidoval na starostu Mariánských Lázní a povedlo se mu to.</p>
        <p>Kampaň samozřejmě spočívá v tom, že kandidující subjekt musí být vidět, musí lidem nabízet svoje řešení a musí s nimi být v neustálém kontaktu. To znamená, že v rámci volební kampaně, která probíhá třeba tři měsíce, musí jít do velmi blízkého kontaktu s lidmi, třeba na ulici a chodí na náměstí, kde se lidé obvykle sdružují. My jsme dělali kampaně ve vestibulech metra, kde chodí spousta lidí, rozdávali jsme Pirátské listy, které jsme sami připravili. Důležité je, aby člověk našel to, co společnost zrovna v ten okamžik potřebuje a prodal jí to.</p>

        <p>
          <b>Měl jste již předem naplánované, co v Praze chcete změnit?</b>
        </p>
        <p>Já bych řekl, že jsme to poměrně dobře odhadli ve volebním programu, takže jsme se k němu vždycky vraceli. Praha má asi dvacet osm akciovek, ve kterých jsou dozorčí rady a představenstva a dosazují se tam političtí kamarádíčci. My jsme nabízeli, že tohle nahradíme otevřenými výběrovými řízeními, kde budou vybráni ti opravdu nejkvalitnější lidi. Nabízeli jsme mnohem větší průhlednost.</p>
        <p>V Praze je třeba velkým tématem nový územní plán, který už se spoustu let připravuje a který rozhodne o tom, kde Praha poroste. Na jednu stranu je potřeba, aby se stavěly nové byty, protože rostou ceny nemovitostí a pro mladou rodinu je těžké dosáhnout na cenově dostupné bydlení. Na druhou stranu je potřeba, aby se stavělo zejména na brownfieldech, ne aby se město rozlézalo do krajiny, protože to má pak ekologický dopad. Musí se prodlužovat metro, obsluha veřejnou dopravou a podobně. Navíc se přichází o zemědělskou půdu. </p>

        <p>
          <b>Máte nějakou radu pro mladé lidi, kteří chtějí kandidovat?</b>
        </p>
        <p>Předně bych doporučil, ať nekandidují, pokud nemají něco, co chtějí pro společnost udělat, co chtějí dělat pro lidi kolem sebe, něco dobrého a poctivého. Protože zásadním problémem politiky je, že se tam hrnou lidi s kariérní vizí a to nedělá žádnou dobrotu.</p>
        <p>Určitě bych doporučil, ať na sobě tvrdě pracují, ať se vzdělávají, ať si nastudují všechny věci, které se týkají toho, co chtějí změnit. Pokud jim vadí, že je naplánovaná nějaká špatná zástavba, tak ať si nastudují věci, které se týkají územního plánu. Pokud jim vadí, že jsou trafiky v dozorčích radách, ať si nastudují věci, které se týkají corporate governance.  Aby to nedopadlo tak, že se vybudí mladí lidé, kteří jsou nezkušení a půjdou do politiky a nakonec tam napáchají ještě větší zlo. Protože mládí samo o sobě není žádné řešení.</p>

        <p>
          <b>Pamatujete si na svůj první den v zastupitelstvu?</b>
        </p>
        <p>Na první den v zastupitelstvu si nepamatuji. Ale měli jsme takový kutloch v zastupitelstvu, protože v Praze má každý politický klub právo na svoje prostory. Tak jsme si ho zvelebili, dali jsme si tam nějaký plakáty a vyvěsili jsme si tam program, abychom nezapomněli, proč nás lidi do zastupitelstva zvolili. A myslím, že se to osvědčilo a pracovalo se nám tam dobře.</p>

      </div>
  },
  'marek-hilser-i-david-muze-porazit-goliase' : {
    date: '12. 4. 2018',
    slug: 'marek-hilser-i-david-muze-porazit-goliase',
    type: 'interview',
    origin: 'vlastní',
    author: 'Jan Bervida',
    image: {
      url: 'https://res.cloudinary.com/farlanis/image/upload/v1525603604/hilser.jpg',
      smallUrl: 'https://res.cloudinary.com/farlanis/image/upload/v1525603604/hilser.jpg'
    },
    videoUrl: 'Iwp5a1t2P_k',
    name: 'Marek Hilšer: “I David může porazit Goliáše. Mládí do politiky zkrátka patří.”',
    text: 'Často se mě na to lidé ptají. Kdybych to měl ve stručnosti shrnout, tak je to as' +
        'i tím, že jsem idealista. Hnala mě dopředu představa ideálního prezidenta pro na' +
        'ši zemi. Nechci tím říct, že ten ideální prezident bych měl být zrovna já, ale c' +
        'htěl jsem prezidenta, který skutečně pracuje pro veřejnost a není loutkou pochyb' +
        'ných zájmových skupin. Ale kde ho na politické scéně najít. Bohužel jsme v situa' +
        'ci, kdy prezident pracuje spíše než pro naši zemi pro cizí...',
    description: <div>
        <p>
          <b>Jak je možné, že jste neztratil motivaci za 2 roky kampaně, kde Vám
            dlouhodobě nikdo nedával šance a Vaše finanční prostředky byly značně
            limitované?</b>
        </p>
        <p>Často se mě na to lidé ptají. Kdybych to měl ve stručnosti shrnout, tak je to
          asi tím, že jsem idealista. Hnala mě dopředu představa ideálního prezidenta pro
          naši zemi. Nechci tím říct, že ten ideální prezident bych měl být zrovna já, ale
          chtěl jsem prezidenta, který skutečně pracuje pro veřejnost a není loutkou
          pochybných zájmových skupin. Ale kde ho na politické scéně najít. Bohužel jsme v
          situaci, kdy prezident pracuje spíše než pro naši zemi pro cizí mocnosti a
          extrémistické strany. To mě velice trápilo a bylo silným motorem, abych se s tím
          jako občan nějak popral. Kromě toho jsem chtěl také ukázat, že by v politice
          neměly rozhodovat jen velké peníze, ale i zájem a dobrá myšlenka něco změnit.
          Kdyby tomu tak nebylo, znamenalo by to, že běžní občané už nemají do politiky co
          mluvit. A to by byl konec demokracie a občanské společnosti. Věřím, že když
          člověk vytrvá, může věci veřejné ovlivnit, i když za sebou nemá nějakou finanční
          či zavedenou politickou strukturu. Trvá to dlouho, jak říkáte, v mém případě
          jsem tomu věnoval dva roky, ale ono nic není zadarmo v tomto ohledu.</p>

        <p>
          <b>Kdybyste to tedy měl spojit s komunální politikou, kde si mladý člověk řekne,
            že má taky tu ideu něco ve městě změnit, ale bojí se toho, že se mu také
            nedostane podpory finanční či politické. Co byste doporučil těmto mladým
            kandidátům?</b>
        </p>
        <p>Musím říci, že prezidentská volba je něco zcela odlišného, než je komunální
          politika. V té aby člověk uspěl, musí být součástí nějaké politické strany nebo
          občanské skupiny, která se rozhodla řešit nějaký konkrétní problém. Neznamená to
          ale, že by mladí měli být na konci kandidátek. V tomto směru to ale mají těžké,
          protože již vytvořené a zaběhané stranické struktury mezi sebe nechtějí pouštět
          někoho nového a třeba mladšího. Mladí lidé se zájmem o politiku, ale nemusí
          začínat hned na kandidátce nějaké strany. Mohou chodit na zastupitelstvo, klást
          otázky a hlídat, aby politici vykonávali svou práci v zájmu obce. Mohou pomáhat
          například jako dobrovolníci. Tak mohou získat zkušenosti a zjistit, jak
          komunální politika funguje. Mohu dát příklad jednoho mladého komunálního
          politika, který se stal nejmladším starostou u nás již v 25 letech. Sice ho
          neznám, ale dočetl jsem se, že se o komunální politiku zajímal už na střední
          škole. Asi byl šikovný, když už tak mladý dostal důvěru a stal se starostou. Je
          to příklad, který ukazuje, že i velice mladí lidé se mohou prosadit. Jak jsem
          již řekl, je nutné se o veřejné věci zajímat, protože jinak se nám stane, že
          nakonec budou rozhodovat jiní o nás bez nás.</p>

        <p>
          <b>Jak důležitou zkušeností pro Vás byla Vaše stáž v poslanecké sněmovně, když
            Vám bylo jednadvacet?</b>
        </p>
        <p>Tady vás asi moc nepotěším. Bylo to v době, prvních velkých politických krizí
          a korupčních skandálů, na konci devadesátých let. Tehdy jsem přišel o mladické
          iluze o politice a rozhodl jsem se, že nechci být na politice existenčně
          závislý. Byla to zkušenost, díky které jsem poznal, že nechci dělat vysokou
          politiku již v brzkém věku, a šel jsem studovat medicínu.Zájem o politiku mě ale
          neopustil. Na lékařské fakultě jsem kandidoval do akademického senátu. Tam jsem
          nakonec strávil 9 let. Ve své podstatě to byla komunální politika, protože
          fakulta a univerzita je svým způsobem obec. Není to sice město nebo vesnice, ale
          také má samosprávu a s kolegy jsme řešili mnoho konkrétních problémů, které nás
          jako studenty trápily.</p>

        <p>
          <b>Dala Vám ta kampaň nějakou zkušenost?</b>
        </p>
        <p>Zase se mi potvrdilo to, co už jsem zjistil předtím prací v akademickém
          senátu. A to, že když se člověk pro něco rozhodne a věří, že to čeho chce
          docílit, má smysl, tak se to nakonec nějakým způsobem dříve nebo později podaří.
          Nemusí se vyplnit úplně vše. Někdy stačí, když otevřete cestu pro jiné. Jako
          studentům se nám v roce 2008 nelíbilo, jak chce vláda měnit statut fakultních
          nemocnic. Měli jsme obavy, že jde o další způsob, jak tunelovat. Tehdy se nás
          šest studentů domluvilo, že na to upozorníme veřejnost. Měsíc jsme pracovali na
          organizaci demonstrace. Dokonce si nás tehdy i pozval ministr, aby nás
          přesvědčil, protože nechtěl, aby se na tento problém veřejně upozorňovalo. Na
          začátku nás bylo šest studentů, pak se přidala univerzita a situaci se nakonec
          podařilo zvrátit. Tam se ukázalo, že i David může porazit Goliáše.</p>

        <p>
          <b>Kdybyste se tedy zamyslel nad tím mladým kandidátem, co by měl ztělesňovat
            kromě vytrvalosti a občanské angažovanosti? Jak by měl oslovit lidi ve svém
            okolí?</b>
        </p>
        <p>Asi cítíme, že mládí není jediným předpokladem dobrého politika. Ve všech
          věkových kategoriích najdeme lidi poctivé a nepoctivé. Politiku je třeba brát
          jako službu. Nesmí to být prostředek, jak dosáhnout osobního prospěchu. Pak
          ztrácí politika svůj smysl a stává se špatným řemeslem. Pokud to jedinec myslí s
          politikou poctivě a nevolí hned jen ty nejjednodušší cesty, tak to nakonec lidi
          v jeho okolí poznají. Většinou to nebývá hned, ale nakonec to přijde.</p>

      </div>
  },
  'jan-grolich-ze-nemate-zkusenosti-starsi-je-taky-nemeli-dokud-to-nezkusili' : {
    date: '18. 4. 2018',
    slug: 'jan-grolich-ze-nemate-zkusenosti-starsi-je-taky-nemeli-dokud-to-nezkusili',
    type: 'interview',
    origin: 'vlastní',
    author: 'Natálie Terčová',
    image: {
      url: 'http://res.cloudinary.com/farlanis/image/upload/v1525600874/grolich.jpg',
      smallUrl: 'http://res.cloudinary.com/farlanis/image/upload/v1525600874/grolich.jpg'
    },
    videoUrl: 'Nuw_HrGpFa0',
    name: 'Jan Grolich: “Že nemáte zkušenosti? Starší je taky neměli, dokud to nezkusili.”',
    text: 'Podporu od kamarádů jsem měl, protože jsme kandidovali celá parta mladých lidí. ' +
        'Rodina se bála, že když kandiduju přímo na starostu, tak si moc věřím a potom bu' +
        'du zklamaný. Ale taky mě podporovali. A u veřejnosti, vzhledem k tomu že mě nako' +
        'nec zvolili, byla podpora taky. Ale to jsem samozřejmě nevěděl předem. Když mě z' +
        'volili, tak tehdy nejstarší zastupitel mi předával řízení zastupitelstva s tím, ' +
        'že předává slovo Honzíkovi. Pro spoustu lidí v obci jsem zůstal Honzík, ale záro' +
        'veň mě respektují jako starostu a funguje to.',
    description: <div>
        <p>
          <b>Kdy vy sám jste se rozhodl kandidovat? Bylo to několik let předem, nebo šlo o
            náhlé rozhodnutí?</b>
        </p>
        <p>Já jsem poprvé kandidoval ve dvaceti šesti letech a hned na starostu.</p>

        <p>
          <b>Co byl prvotní impulz?</b>
        </p>
        <p>U nás to vyplynulo ze situace. Předchozí vedení kompletně končilo a museli
          přijít úplně noví lidé. Já jsem chtěl kandidovat do zastupitelstva. Hledal se
          kandidát na starostu a, jak já říkám, lepší se nenašel.</p>

        <p>
          <b>Jak na to reagovala rodina a kamarádi? Chtěli vás odradit, nebo vás naopak
            podporovali?</b>
        </p>
        <p>Podporu od kamarádů jsem měl, protože jsme kandidovali celá parta mladých
          lidí. Rodina se bála, že když kandiduju přímo na starostu, tak si moc věřím a
          potom budu zklamaný. Ale taky mě podporovali. A u veřejnosti, vzhledem k tomu že
          mě nakonec zvolili, byla podpora taky. Ale to jsem samozřejmě nevěděl předem.
          Když mě zvolili, tak tehdy nejstarší zastupitel mi předával řízení
          zastupitelstva s tím, že předává slovo Honzíkovi. Pro spoustu lidí v obci jsem
          zůstal Honzík, ale zároveň mě respektují jako starostu a funguje to.</p>

        <p>
          <b>Přece jen se v tom promítl věk, ve kterém jste kandidoval…</b>
        </p>
        <p>Pořád se stává, že lidé jdou na úřad, přijdou do mé kanceláře a hledají
          starostu. Nedojde jim, že s ním už mluví. A když jim řeknu: „To jsem já,“ tak se
          diví: „Tak mladej?“ Obchoďáci toho zneužívají a hned začnou chválit, jak je to
          super. Ale je spousta lidí, kteří jdou řešit nějaký problém a nemají zkušenosti
          s oficiálním jednáním a z těch to vždycky vypadne tak upřímně překvapeně. Ale
          potom se vždycky všechno vyřeší.</p>

        <p>
          <b>Pamatujete si svůj první den v roli starosty?</b>
        </p>
        <p>Ten první den byl hlavně o formalitách. Předchozí starosta mi předával
          razítka a podobně. Byl to zvláštní pocit, protože, co si já pamatuju, tak tam
          byl vždy stejný starosta. A když jsme něco řešili na úřadě, tak tam byl pořád
          on. Já to bral, jako takové jeho království. A dodnes tam jsou jeho složky, z
          respektu na ně nechci sahat a dělat mu v nich nepořádek. Za některými věcmi jsem
          udělal tlustou čáru a začal jsem je řešit po svém. Ale ty jeho papíry tam pořád
          nechávám, přesouvám z archivu do archivu a nehrabu do nich. Z respektu k lidem,
          co to dělali přede mnou.</p>

        <p>
          <b>Jak probíhá takový Váš běžný den?</b>
        </p>
        <p>To je nejčastější otázka a vůbec se na ni nedá odpovědět. A to je přesně to, co se mi na téhle práci líbí, každý ten den je úplně jiný. Například když realizujete stavbu, tak se vám stane, že jdete zkontrolovat bagrování a máte zaplácané boty od bahna. Pak je odrhnete někde na chodníku, hodíte na sebe sako a jedete řešit něco na kraj. Od té základní práce až po důležitá jednání je tam toho strašně moc a každý den je jiný. Líbí se mi, ale to funguje asi jen na malé obci, že dělám to, co sám vymyslím. Když něco naplánuju a není to úplný nesmysl, tak se to realizuje. Takže já si dopředu plánuju věci, které chci dělat a všední práci si můžu okořenit vlastními nápady a menšími projekty.
        </p>

        <p>
          <b>Co byste mladým kandidátům doporučil? Koho mají kontaktovat, jak se k tomu
            postavit, co je prvotní krok a impulz.</b>
        </p>
        <p>Já myslím, že jsou dva hlavní důvody proč kandidovat. Pokud vás štve, jak to
          někdo dělá, pak má smysl postavit kandidátku, oslovit kamarády a jít do toho s
          novou silou a myšlenkou. A pokud se vám líbí, jak to někdo dělá a chcete ho
          podpořit, tak se vůbec nebojte ho oslovit. Myslím, že ve všech obcích se i ti
          starší starostové a vedení snaží mladé přitáhnout, akorát jim chybí přímý
          kontakt a neví koho oslovit. Takže pokud do toho máte chuť, nebojte se, podle mě
          budou rádi, když za nimi přijdete.</p>

        <br/>
        <p>
          <b>Pokud Vás starosta Jan Grolich zaujal, více se o něm a jeho starostování
            dozvíte na jeho cool stránkách
            <a href="http://coolstarosta.cz/" target="_blank">coolstarosta.cz</a>.</b>
        </p>

      </div>
  },
  '101-drobnosti-jak-zlepsit-sve-mesto' : {
    date: '1. 5. 2018',
    slug: '101-drobnosti-jak-zlepsit-sve-mesto',
    type: 'article',
    origin: 'https://www.curbed.com/2016/9/22/13019420/urban-design-community-building-placem' +
        'aking',
    image: {
      url: 'https://res.cloudinary.com/farlanis/image/upload/v1525597362/article2-min.png',
      smallUrl: 'https://res.cloudinary.com/farlanis/image/upload/v1525597362/article2-min.png'
    },
    name: '101 drobností, kterými můžeš zlepšit své město',
    text: 'Nemusíš mít miliony ani stát v čele vítězné strany, abys mohl zlepšovat své měst' +
        'o, postavit třeba most nebo třeba zařídit venkovní promítání. Přitom stačí troch' +
        'a kreativity, pár plechovek barev, trocha dřeva a šikovnosti.',
    description: <div>
        <p>Nemusíš mít miliony ani stát v čele vítězné strany, abys mohl zlepšovat své
          město, postavit třeba most nebo třeba zařídit venkovní promítání. Přitom stačí
          trocha kreativity, pár plechovek barev, trocha dřeva a šikovnosti.</p>
        <p>V následujícím článku najdeš kupu typů, mezi kterými si každá kreativní duše
          najde to své.</p>
        <p>
          <a
            href="https://www.curbed.com/2016/9/22/13019420/urban-design-community-building-placemaking"
            target="_blank">https://www.curbed.com/2016/9/22/13019420/urban-design-community-building-placemaking</a>
        </p>
      </div>
  },
  'nszm-celostatni-seminar-chytre-a-udrzitelne-mesto': {
    date: '5. 5. 2018',
    slug: 'nszm-celostatni-seminar-chytre-a-udrzitelne-mesto',
    type: 'news',
    origin: 'https://www.zdravamesta.cz/index.shtml?apc=r2398016n',
    image: {
      url: 'https://res.cloudinary.com/farlanis/image/upload/v1525597353/article1-min.jpg',
      smallUrl: 'https://res.cloudinary.com/farlanis/image/upload/v1525597353/article1-min.jpg'
    },
    name: 'NSZM chystá celostátní seminář "Chytré a udržitelné město"',
    text: 'Ve středu 22. května 2018 proběhne v Praze celostátní seminář Chytré a udržiteln' +
        'é město. Přijďte i Vy společně diskutovat nad možnostmi smart řešení pro udržite' +
        'lné municipality a regiony. Záštitu na akcí převzala Klára Dostálová, ministryně' +
        ' pro místní rozvoj.',
    description: <div>
        <p>Ve středu 22. května 2018 proběhne v Praze celostátní seminář Chytré a
          udržitelné město. Přijďte i Vy společně diskutovat nad možnostmi smart řešení
          pro udržitelné municipality a regiony. Záštitu na akcí převzala Klára Dostálová,
          ministryně pro místní rozvoj.</p>
        <p>Vystoupí nejen zástupci ministerstev a další renomovaní prezentující a
          experti, kteří se této problematice aktivně věnují, ale také praktici z měst a
          krajů. Program je zaměřen mj. na tato témata:</p>
        <ul>
          <li>Jaké inovace mohou obce využívat v oblasti dopravy a energetiky?</li>
          <li>Jaké jsou zásady chytrého finančního řízení?</li>
          <li>Odkud čerpat inspiraci a dobrou praxi?</li>
        </ul>
        <p>Akce je určena všm zájemcům z měst a regionů, ale i spolupracujícím
          organizacím. Účast na akci není zpoplatněna.</p>
        <p>Více o akci a registraci najdete zde:
          <a href="https://www.zdravamesta.cz/index.shtml?apc=r2398016n" target="_blank">https://www.zdravamesta.cz/index.shtml?apc=r2398016n</a>
        </p>
      </div>
  },
  'michal-picl-bez-angazovanosti-mesta-nemohou-fungovat' : {
    date: '13. 4. 2018',
    slug: 'michal-picl-bez-angazovanosti-mesta-nemohou-fungovat',
    type: 'interview',
    origin: 'vlastní',
    author: 'Šárka Löffelmannová',
    image: {
      url: 'https://res.cloudinary.com/farlanis/image/upload/v1525603646/picl.jpg',
      smallUrl: 'https://res.cloudinary.com/farlanis/image/upload/v1525603646/picl.jpg'
    },
    videoUrl: 'zoBHwxFNqlQ',
    name: 'Michal Pícl: “Bez angažovanosti města nemohou fungovat.”',
    text: 'Lidé mohou měnit svět i mimo politiku pomocí občanské angažovanosti. Nekončím tí' +
        'm, že nejsem zvolen do nějaké funkce,  já můžu měnit svět i v různých spolcích a' +
        ' aktivistických uskupeních. I tohle může člověka obohacovat. ',
    description: <div>
        <p>
          <b>Kdy vy sám jste se rozhodl kandidovat? Bylo to několik let předem, nebo šlo o
            náhlé rozhodnutí?</b>
        </p>
        <p>Já mám sociální demokracii spojenou s rodinou historií. Můj otec byl
          starostou, poslancem i náměstkem ministryně. Takže jsem vyrůstal v politické
          rodině a ve svém otci stále vidím určitý vzor. To on mě nasměroval k sociální
          demokracii, jejíž hodnoty zastávám. Poprvé jsem kandidoval v minulém volebním
          období do komunálu, bohužel jsme jako sociální demokraté na Praze 4 úplně
          neuspěli a teď se to budeme snažit změnit.</p>

        <p>
          <b>Když jste se nedostal do zastupitelstva, co uděláte letos jinak a co byste
            doporučil lidem, kterým se stane to samé?</b>
        </p>
        <p>Tak důležitý je nevzdat to při prvním neúspěchu. Dneska se kampaň úplně
          proměnila oproti tomu, co bylo před čtyřmi, před osmi, dvanácti, šestnácti lety.
          Dneska kampaň vypadá úplně jinak. Už to není o tom, že se dělají velké meetingy
          na náměstí. Ale především musíte celé ty čtyři roky komunikovat s lidmi, být
          mezi nimi. Není to jen o tom, že jednou za čas někam přijdete, ale musíte být
          opravdu v permanentním kontaktu, ať už na sociálních sítích, nebo na různých
          akcích, které probíhají ve vašem městě, městské části, nebo zrovna tam, kam
          chcete kandidovat. Lidé vás musí znát, aby se mohli identifikovat s vašimi
          názory, s vaším politickým programem, aby věděli, proč mají volit právě vás.</p>

        <p>
          <b>Jste členem ČSSD, kde není tolik mladých aktivních lidí jako třeba v jiných
            stranách. Myslíte si, že tradiční strany mladé lidi tolik nelákají?</b>
        </p>
        <p>Já si myslím, že ano. My máme v rámci sociální demokracie spolek mladých
          sociálních demokratů, který sdružuje mladé napříč republikou. Strana tento
          spolek finančně podporuje. Takže přípravku pro velkou politiku strana má a
          podporuje.</p>

        <p>
          <b>Doporučil byste mladým lidem, kteří zvažují kandidaturu, vstup do stranického
            spolku?</b>
        </p>
        <p>
          Určitě, je to pro mladé lidi dobrá možnost jak k politice přičichnout a zároveň
          pokud se ještě necítí na to jít do politiky naplno, tak tyto spolky pořádají
          různé společenské akce, výlety, semináře a podobně.Člověk se tak může postupně
          seznamovat s politickým životem.</p>

        <p>
          <b>S čím konkrétně jste šel do politiky vy, co jste chtěl změnit?</b>
        </p>
        <p>Já bydlím na Praze 4, kde jsem i kandidoval. V době před volbami jsme byli
          součástí vedení a chtěli jsme pokračovat v rozdělané práci. To znamená dostavbu
          domu seniorů Hudečkova nebo zklidnění situace na Praze 4 na Spořilově a v
          Podolí. To jsou problémy, které se týkají přímo městské části, ve které pobývám.
          Navíc se zabývám i celostátní politikou, to znamená celorepublikovými tématy,
          jako je nezaměstnanost, hospodářská situace a tak podobně. To je dáno i tím, že
          jsem vystudovaný národohospodář a tyhle věci mě baví.</p>

        <p>
          <b>Jací by měli mladí lidé být, aby v politice uspěli a nesemlelo je to? Jak se
            mohou udržet v zavedených politických strukturách?</b>
        </p>
        <p>Z velké části jsou na předních příčkách lidé, kteří jsou již nějakým způsobem
          zavedení a mají dlouhodobé zkušenosti s politikou. Takže je určitě dobré se s
          někým takovým spojit a mít ho jako takového mentora, svého politického průvodce,
          aby se v tom světě úplně neztratili. Určitě je důležité, aby se zabývali
          problémy, které jsou kolem nich a snažili se je řešit. Není to jen o tom, že
          budu zvolen do zastupitelstva a v nějaké funkci budu něco měnit. Lidé mohou
          měnit svět i mimo politiku pomocí občanské angažovanosti. Nekončím tím, že
          nejsem zvolen do nějaké funkce, já můžu měnit svět i v různých spolcích a
          aktivistických uskupeních. I tohle může člověka obohacovat.
        </p>

      </div>
  },
  'aktivista-vs-zastupitele-student-uspel-s-zalobou': {
    date: '27. 4. 2018',
    slug: 'aktivista-vs-zastupitele-student-uspel-s-zalobou',
    type: 'article',
    origin: 'https://ustecky.denik.cz/zpravy_region/aktivista-vs-zastupitele-1-0-student-uspe' +
        'l-s-zalobou-na-vedeni-mesta-usti-20180209.html',
    image: {
      url: 'https://res.cloudinary.com/farlanis/image/upload/v1525598300/article3.jpg',
      smallUrl: 'https://res.cloudinary.com/farlanis/image/upload/v1525598300/article3.jpg'
    },
    name : 'Aktivista vs. zastupitelé: 1:0 Student uspěl s žalobou na vedení města Ústí',
    text: 'Sdílíme zajímavý případ z Ústeckého kraje. “Aktivista Lukáš Blažej uspěl u soudu' +
        ' s žalobou proti Zastupitelstvu města Ústí nad Labem. Vadilo mu, že zastupitelst' +
        'vo dlouhodobě neumožňovalo občanům vystoupit se svým příspěvkem v rozpravě k pro' +
        'gramu jednání a diskusi přesouvalo do závěrečného bodu „Různé“. Často se však st' +
        'ávalo, že většina zastupitelů zasedání před koncem opustila a jednání bylo neusn' +
        'ášeníschopné.”',
    description: <div>
        <p>Sdílíme zajímavý případ z Ústeckého kraje. “Aktivista Lukáš Blažej uspěl u
          soudu s žalobou proti Zastupitelstvu města Ústí nad Labem. Vadilo mu, že
          zastupitelstvo dlouhodobě neumožňovalo občanům vystoupit se svým příspěvkem v
          rozpravě k programu jednání a diskusi přesouvalo do závěrečného bodu „Různé“.
          Často se však stávalo, že většina zastupitelů zasedání před koncem opustila a
          jednání bylo neusnášeníschopné.”</p>
        <p>Zkrátka, jak jsme několikrát zmiňovali v naší informační sekci,otevřenost
          zastupitele a celé radnice se zkrátka nejen vyplácí, je to do určité míry ze
          zákona nezbytností. Pokud tě zajímají detaily, najdeš je v článku k případu od
          Ústeckého Deníku.</p>
        <p>
          <a
            href="https://ustecky.denik.cz/zpravy_region/aktivista-vs-zastupitele-1-0-student-uspel-s-zalobou-na-vedeni-mesta-usti-20180209.html"
            target="_blank">https://ustecky.denik.cz/zpravy_region/aktivista-vs-zastupitele-1-0-student-uspel-s-zalobou-na-vedeni-mesta-usti-20180209.html</a>
        </p>
      </div>
  },
}