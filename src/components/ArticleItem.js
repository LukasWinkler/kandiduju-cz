import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import { Link } from 'react-router-dom'
import { ArticleType } from './'

import './ArticleItem.css'

const ArticleItem = ({ article }) => {

  return (
    <div className="article-item-wrapper">
      <article className="article-item">
        <div className="article-item__image">
          <img alt={article.name} src={article.image.url} />
        </div>
        <div className="article-item__content">
          <div className="title-date">
            <div className="title-date__title">
              {article.name}
            </div>
            <div className="title-date__date">
              {article.date}
            </div>
          </div>
          <div className="description-art">
            {article.text}
          </div>
          <div className="tag-link">
            <ArticleType type={article.type} />
            <div className="link">
              <Link to={`/clanky/${article.slug}`}>
                {'Přečíst celé'}
              </Link>
            </div>
          </div>
        </div>
      </article>
    </div>
  )
}

ArticleItem.propTypes = {
  article: PropTypes.object.isRequired,
}

export default ArticleItem
