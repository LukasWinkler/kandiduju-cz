import React from 'react'
//import { Link } from 'react-router-dom'
import { Section, InfoItem } from '../'
import './InformationSection.css'
import zastupitelstvo from '../../images/zastupitelstvo.png'
import mojeobec from '../../images/mojeobec.png'
import povolbach from '../../images/povolbach.png'
import chcikandidovat from '../../images/chcikandidovat.png'

const infIt = [
  {
    title: 'Moje obec - jak funguje?',
    text: 'Jak vlastně obce fungují a o čem mohou zastupitelé rozhodovat? Připravili jsme pro tebe základní informační balíček, který ti sdělí to nejdůležitější.',
    image: mojeobec,
    link: '/informace#moje-obec',
  },
  {
    title: 'Zastupitelstvo - jak funguje?',
    text: 'Jak funguje ten nejklíčovější politický orgán obcí? Jaké má zastupitel pravomoce a co pro mě mandát zastupitele bude znamenat?',
    image: zastupitelstvo,
    link: '/informace#zastupitelstvo',
  },
  {
    title: 'Chci kandidovat!',
    text: 'Zaručený recept na úspěch ve volbách neexistuje, přesto je dobré vědět, jak mu jít co nejlépe naproti. Přinášíme ti rady a informace k volebnímu systému a ke strategii kampaně.',
    image: chcikandidovat,
    link: '/informace#chci-kandidovat',
  },
  {
    title: 'Co po volbách?',
    text: 'Získal jsi mandát a přemýšlíš, jak ho co nejlépe využít ke zlepšení svého města? Nebo jsi mandát nezískal, ale přesto tam chceš aktivně působit? Najdeš tu např. tipy k financování svých nápadů a zásady poctivého zastupitele.',
    image: povolbach,
    link: '/informace#po-volbach',
  },
]

const InformationSection = () => (
  <Section title="Informace" type="white" >
    <div className="information-section">
      {infIt.map(ii => (
        <InfoItem
          title={ii.title}
          text={ii.text}
          image={ii.image}
          link={ii.link}
        />
      ))}
    </div>
  </Section>
)

export default (InformationSection)
