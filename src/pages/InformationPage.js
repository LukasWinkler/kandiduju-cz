import React from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import { PageWrapper } from './'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import './InformationPage.css'
import ipm from '../images/institut.png'
import {landImgs} from '../config'
const hashes = ['#moje-obec', '#zastupitelstvo', '#chci-kandidovat', '#po-volbach']

class InformationPage extends React.Component {
  componentDidMount() {
    document.title = "Informace | Kandiduju.cz"
    window.scrollTo(0, 0)
    const { location, history } = this.props
    if (location && location.hash) {
      if (hashes.indexOf(location.hash) < 0 ) {
        history.push('/informace#moje-obec')
      }
   } else {
      history.push('/informace#moje-obec')
    }
  }

  handleTabChange = (hash) => {
    const { history } = this.props
    history.push(`/informace${hash}`)
  }

  render() {
    const { location } = this.props
    const { handleTabChange } = this

    const tab = location && location.hash && hashes.indexOf(location.hash) >= 0
      ? location.hash
      : '#moje-obec'

    return (
      <PageWrapper title="Informace" image={landImgs.land2}>
        <div className="information-page container">
          <div className="information-page__menu">
            <div
              className={`tab-button${tab === '#moje-obec' ? ' tab-button--active' : ''}`}
              onClick={() => handleTabChange('#moje-obec')}
            >
              Moje obec
            </div>
            <div
              className={`tab-button${tab === '#zastupitelstvo' ? ' tab-button--active' : ''}`}
              onClick={() => handleTabChange('#zastupitelstvo')}
            >
              Zastupitelstvo
            </div>
            <div
              className={`tab-button${tab === '#chci-kandidovat' ? ' tab-button--active' : ''}`}
              onClick={() => handleTabChange('#chci-kandidovat')}
            >
              Chci kandidovat
            </div>
            <div
              className={`tab-button${tab === '#po-volbach' ? ' tab-button--active' : ''}`}
              onClick={() => handleTabChange('#po-volbach')}
            >
              Po volbách
            </div>
          </div>
          <div className="information-page__tabs">
          {tabs[tab]}
          </div>
        </div>
      </PageWrapper>

    )
  }
}

const enhance = compose(
  withRouter
)

export default enhance(InformationPage)


const tabs = {
  '#moje-obec':
  <article className="tab">
    <h2 className="tab__title">Moje obec - jak funguje?</h2>
    <i className="tab__abstract">Určitě přemýšlíš, jak vlastně obce fungují a o čem mohou zastupitelé rozhodovat. Pokud máš chuť a čas, pak se můžeš prokousat zákonem č. 128/2000 Sb., tedy zákonem o obcích. V něm nalezneš základní zakotvení obcí, které samozřejmě vychází ze samotné ústavní úpravy. Avšak nezoufej, připravili jsme pro tebe základní informační balíček, který ti sdělí to nejdůležitější.</i>
    <p><b>Obce</b> jsou základním územním samosprávným společenstvím občanů – mohou vlastnit majetek a zejména pečují o všestranný rozvoj svého území a o potřeby svých občanů. To je základní zákonné vymezení obcí. Jak vidíš, obce tu jsou pro své občany, kteří by se měli zajímat o dění ve své obci a pečovat o veřejný prostor kolem sebe.</p>
    <p>Mezi nejdůležitější orgány obce patří zastupitelstvo, rada, starosta, obecní úřad a další zvláštní orgány obce. Pokud má obec alespoň 3000 obyvatel, pak předseda Poslanecké sněmovny po vyjádření vlády a na návrh obce může stanovit, že je taková obec městem. Orgány jsou stále stejné, jen se jinak jmenují – zastupitelstvo města, rada města a tak dále.</p>
    <p>Zákon též stanovuje, která města jsou <b>statutárními</b>. Od „běžných měst“ se liší tím, že jejich území se člení na městské části či městské obvody s vlastními orgány samosprávy. Jedná se například o Kladno, České Budějovice, Plzeň, Karlovy Vary, Ústí nad Labem, Liberec atd… V komunálních volbách se v těchto městech bude volit do zastupitelstva města, ale i do zastupitelstev jednotlivých částí a obvodů.</p>
    <h3 className="tab__subtitle">Jaké orgány obce mají?</h3>
    <p><b>Rada obce</b> je výkonným orgánem obce a ze své činnosti odpovídá zastupitelstvu. V menších obcích (tam, kde má zastupitelstvo méně než 15 členů) se ale rada nevolí a její činnost vykonává starosta. Zastupitelstvo volí radu ze svých členů a může ji taktéž odvolat, ale pozor – zastupitelstvo může odvolat i jednotlivé členy rady. A co tedy rada dělá? Například zabezpečuje hospodaření obce podle schváleného rozpočtu, kontroluje činnost obecního úřadu… Zkrátka vykonává zejména exekutivní činnost</p>
    <p><b>Starosta obce</b> pak zastupuje obec navenek. Je jejím prvním představitelem. Zajímalo by tě, jak se stát starostou? Nejprve se musíš dostat do zastupitelstva, protože starostu volí zastupitelstvo z řad jeho členů. Starosta řídí jednání zastupitelstva a rady, odpovídá za informování veřejnosti obce… Ale platí, že nejdůležitějším orgánem obce je samotné zastupitelstvo, protože rozhoduje o nejdůležitějších otázkách a volí ostatní orgány.</p>
    <p>Podstatné je si uvědomit, že obce vykonávají <b>samostatnou a přenesenou působnost</b>. Co to znamená? V samostatné působnosti si obec spravuje sama vlastní záležitosti, které jsou v zájmu obce a jejích občanů. Rozhoduje tak například o opravě místních komunikací, zřízení obecní policie atd. V přenesené působnosti pak obec vykonává státní správu – stát na obec přenáší některé činnosti – např. vydávání občanských průkazů, stavební povolení atd.</p>
    <p>Obce samozřejmě vydávají i právní předpisy. Jedná se o <b>obecně závazné vyhlášky</b>, kterými mohou v samostatné působnosti ukládat i povinnosti, pokud se to týká zejména veřejného pořádku, udržování čistoty veřejných prostranství či pořádání, průběhu a ukončení tančeních zábav, diskoték a dalších. Nařízení pak vydává v přenesené působnosti, je-li k tomu zákonem zmocněna. Zároveň nařízení musí být vydáno jen na základě zákona a v jeho mezích.</p>
  </article>,

  '#zastupitelstvo':
    <article className="tab">
      <h2 className="tab__title">Zastupitelstvo – kam můžu kandidovat?</h2>
      <i className="tab__abstract">Teď, když jsme si představili základy fungování a činnosti obcí, se můžeme podrobně podívat na zastupitelstvo obce. Ten nejdůležitější orgán, do kterého na podzim proběhnou volby. </i>
      <p><b>Zastupitelstva obcí jsou různě početná</b>. Kolik zastupitelů bude v nadcházejících volbách zvoleno, stanovuje stávající zastupitelstvo nejpozději do 85 dnů před dnem voleb. Zákon stanovuje rozmezí počtu zastupitelů podle velikosti obce, resp. dle počtu obyvatel. Takže například obec s 600 obyvateli může mít 7, ale klidně až 15 zastupitelů…</p>
      <p><b>Mandát člena zastupitelstva ti vzniká zvolením</b>. Jak se nechat zvolit a jak probíhají samotné volby se dozvíš dále na naší stránce. Na prvním zasedání zastupitelstva, kterého se po svém zvolení zúčastníš, pak budeš muset složit slavnostní slib.</p>
      <p><b>Zákon pak rozlišuje zastupitele uvolněné a neuvolněné</b>. Uvolnění členové zastupitelstva jsou dlouhodobě uvolněni z pracovního poměru a náleží jim pak odměna vyplácená z rozpočtu obce. Avšak nemysli si, že odměna uvolněného člena zastupitelstva dosahuje závratných výšin. Odvozuje se podle velikosti obce. A konečně neuvolnění členové zastupitelstva pak i nadále zůstávají v pracovním poměru, těm zaměstnavatel poskytne pracovní volno s náhradou mzdy (tu zaměstnavateli uhradí obec). Ale závisí jen na vůli zastupitelstva, jestli měsíční odměnu poskytne též „neuvolněným zastupitelům“.</p>
      <p><b>A co tedy může zastupitel?</b> Předně má právo předkládat zastupitelstvu, radě, výborům a komisím návrhy na projednání, vznášet dotazy, připomínky a podněty na radu obce a její jednotlivé členy, dále požadovat od zaměstnanců obce zařazených do obecního úřadu, jakož i od zaměstnanců právnických osob založených obcí informace, které souvisejí s výkonem jejich funkce.</p>
      <p>Ale pozor! Zastupitel má i povinnosti, viz § 83 zákona o obcích: <b>Člen zastupitelstva obce je povinen zúčastňovat se zasedání zastupitelstva obce, popřípadě zasedání jiných orgánů obce, je-li jejich členem, plnit úkoly, které mu tyto orgány uloží, hájit zájmy občanů obce a jednat a vystupovat tak, aby nebyla ohrožena vážnost jeho funkce.</b></p>
      <p>Teď si ještě ujasníme, <b>o jakých záležitostech má pravomoc rozhodovat zastupitelstvo obce</b>. Může schvalovat strategické dokumenty jako například rozvojový plán obce, schvaluje rozpočet obce, rozhoduje o založení právnických osob („obecních firem“), vydává obecně závazné vyhlášky, zřizuje obecní policii a spoustu dalšího. Pokud tě spoluobčané zvolí, pak se zejména <a href="http://zakony.centrum.cz/zakon-o-obcich/cast-1-hlava-4-dil-2" target="_blank">tuto pasáž zákona</a> nauč.</p>
    </article>,

  '#chci-kandidovat': <article className="tab">
    <h2 className="tab__title">Chci kandidovat</h2>
    <div className="tab__author"><h4>Autor sekce: Institut politického marketingu</h4><a href="http://politickymarketing.com/" target="_blank"><img src={ipm} alt="Institut politického marketingu" /></a></div>
    <i className="tab__abstract">Velmi důležitou otázkou je, jakým způsobem uspět ve volbách. Na to bohužel neexistuje ani nemůže existovat obecně aplikovatelný seznam zaručených rad, vždy je to věcí dost individuální. Přesto je dobré znát alespoň základní důležité poučky a technikálie ke strategii kampaně a k systému komunálních voleb.</i>
    <p>Ve volbách do obecních zastupitelstev se používá <b>volební systém listinného poměrného zastoupení</b>. Aplikuje se <b>pětiprocentní volební klauzule</b> (tzn. pokud nedosáhnete alespoň na 5 % hlasů, vaše kandidátka nemůže získat žádný mandát) a mandáty (křesla) se distribuují na základě D'Hondtova dělitele (ve variantě 1, 2, 3, 4, 5 až počet volených zastupitelů).</p>
    <p>Každý volební subjekt smí předložit kandidátní listinu s počtem kandidátů maximálně rovnajícímu se počtu rozdělovaných křesel. Počet členů zastupitelstva stanovuje stávající zastupitelstvo v mezích zákona nejpozději 85 dnů před volbami, ale k takovým změnám často nedochází. Proto je vhodné sehnat tolik kandidátů, kolik je členů zastupitelstva.</p>
    <p>Velmi důležitou informací je, <b>že každý volič disponuje počtem hlasů, který se rovná počtu volených členů zastupitelstva</b>.</p>
    <h3 className="tab__subtitle">Voliči mají tři možnosti, jakým způsobem hlasovat.</h3>
    <ol>
      <li>
        <p><b>Vyberou si jednu kandidátku a té dají všechny své hlasy</b></p>
        <ul>
          <li>Toto je pro Tvůj volební subjekt optimální a měli byste k takovému hlasování voliče vybízet.</li>
        </ul>
      </li>
      <li>
        <p><b>Vyberou si jednu kandidátku a doplní ji o kandidáty z jiných kandidátek</b></p>
        <ul>
          <li>Toto je pro vás druhá nejlepší varianta (protože pokud volič kromě vaší kandidátky hlasuje jen pro malý počet jiných kandidátů, získáváte stále většinu jeho hlasů).</li>
        </ul>
      </li>
      <li>
        <p><b>Nevyberou si žádnou celou kandidátku, ale své hlasy přidělí jednotlivým kandidátům z libovolného počtu kandidátních listin.</b> (Počet takto vybraných kandidátů ale nesmí překročit počet rozdělovaných mandátů, jinak se hlasovací lístek stává neplatným).</p>
        <ul>
          <li>Toto je pro vás nejméně příhodný výsledek. V součtu nakonec získáte mnohem méně hlasů. Hlasy se navíc tříští, což není obzvláště pro malé strany výhodné. Voliče byste rozhodně měli od tohoto způsobu hlasování odrazovat.</li>
        </ul>
      </li>
    </ol>
    <h3 className="tab__subtitle">A jaká jsou doporučení, jak tento volební systém využít ve svůj prospěch?</h3>
    <ol>
      <li>
        <p><b>Vždy mějte kandidátní listinu plnou. I kdybyste ji měli zaplnit statisty. Pokud nebudete mít na kandidátce maximální možný počet kandidátů, získáte při stejném počtu voličů daleko méně hlasů než konkurující strany s plně obsazenými kandidátkami.</b></p>
      </li>
      <li>
        <p><b>Nedomlouvejte se s žádným jiným hnutím či stranou na vzájemné podpoře a nenabádejte voliče, aby si vybírali kandidáty volně z vašich dvou kandidátek. Povede to k zisku menšího počtu hlasů pro oba subjekty.</b></p>
      </li>
      <li>
        <p>Pokud vám jde o prosazení konkrétních kandidátů, dejte je na <b>čelní místa kandidátky</b>. Na kroužkování (v případě komunálních voleb spíše křížkování) raději nespoléhejte.</p>
      </li>
      <li>
        <p>Pokud existuje ve vaší obci druhé uskupení, které je vám ideově či jinak blízké, je <b>rozumné kandidovat spolu</b>. A to ze tří důvodů:</p>
        <ul>
          <li>Podaří se vám zaplnit kandidátku více kvalitními osobnostmi, a tak je možné, že vás více voličů zaškrtne jako svou volbu pro celou stranu než v případě, že byste kandidovali jako dvě nezávislá uskupení.</li>
          <li>Je větší šance, že překročíte volební klauzuli pěti procent. Pokud obě uskupení získají samostatně každá čtyři procenta, ani jedno z nich se do zastupitelstva nedostane.</li>
          <li>Protože volební systém zvýhodňuje strany s vyššími zisky (přiděluje jim více mandátů), získáte se společnou kandidátkou pravděpodobně více křesel, než by získaly kandidátky oddělené (i když by součet získaných hlasů byl stejný.</li>
        </ul>
      </li>
      <li>
        <p>V menších obcích se více soustřeďte na jednotlivé kandidáty, ve větších na celé kandidátky a na lídry.</p>
      </li>
    </ol>
    <h3 className="tab__subtitle">Kandidátní listina a jak vlastně kandidovat</h3>
    <p>Nejprve musíš kandidátní listinu sestavit a zaregistrovat ji. Nutno dodat, že máš několik možností, jak kandidovat:</p>
    <ul>
      <li>
        <p><b>Za existující politickou stranu</b></p>
        <ul>
          <li>Z praktického hlediska je to nejjednodušší varianta – v případě již existující strany není nutné žádných právních úkonů a kandidátku je možné hned sestavit a nechat zaregistrovat</li>
        </ul>
      </li>
      <li>
        <p><b>Jako sdružení nezávislých kandidátů</b></p>
        <ul>
          <li>V tomto případě je nutné sehnat podpisy 7 % občanů vaší obce, kteří svým podpisem kandidátku podpoří.</li>
        </ul>
      </li>
      <li>
        <p><b>Jako nezávislá kandidátka</b></p>
        <ul>
          <li>Chcete-li se vyhnout sbírání podpisů, i v tom případě pro vás existuje řešení. To spočívá v kandidatuře pod hlavičkou již existující strany v České republice jako nezávislá kandidátka.</li>
          <li>Následně stačí, když bude minimálně jeden z vašich kandidátů touto stranou na vaši kandidátku tzv. „nominován“, a následně je možné vytvořit nezávislou kandidátku, která nemá s původní „nominační“ stranou nic společného, včetně názvu nebo kandidátů.</li>
        </ul>
      </li>
      <li>
        <p><b>Jako jednotlivec</b></p>
        <ul>
          <li>Ano, můžeš kandidovat i jako jednotlivec. To ale vzhledem k povaze volebního systému vůbec nedoporučujeme, protože budeš mít nulovou šanci na zvolení.</li>
        </ul>
      </li>
    </ul>
    <h3 className="tab__subtitle">A jaké jsou náležitosti kandidátní listiny?</h3>
    <ol>
      <li>Na kandidátní listinu můžete zapsat <b>občan České republiky s trvalým místem pobytu v místě kandidatury, kterým je nejpozději v době konání voleb 18 let</b>.</li>
      <li>Ideální a doporučované je <b>naplnit kandidátku kompletně</b>, tzn. mít na kandidátce tolik kandidátů, kolik je míst v zastupitelstvu. V opačném případě totiž při každém zakroužkování celé vaší kandidátky voličem ztrácíte do celkového počtu hlasů a tedy do volebního výsledku tolik hlasů, kolik kandidátů vám na kandidátce chybí do celkového počtu.</li>
      <li>Jak již bylo řečeno, ve většině případů je nutné pro registraci vaší kandidátky získat určitý zákonem stanovený <b>počet podpisů spoluobčanů</b>. Ač se může tento úkon zdát jako zdržující a nepříjemný, na druhou stranu vám může přinést již na startovní čáru náskok před ostatními subjekty.</li>
      <li>Kompletní kandidátní listiny se potom <b>podávají buďto k místnímu volebnímu úřadu, nebo k pověřenému obecnímu úřadu</b>. To naleznete na úřední desce dané obce.</li>
      <li>Registrační úřad potvrdí podání kandidátní listiny zmocněnci volební strany (§ 22 odst. 4) nebo nezávislému kandidátovi.</li>
      <li><b>Součástí podání musí být petiční archy podepsané příslušným počtem voličů v dané obci</b> (je-li nutné z podstaty vaší kandidátní listiny), <b>vlastnoručně podepsaná prohlášení všech kandidátů, samotná kandidátní listina a ustanovení zmocněnce a jeho náhradníka</b>.</li>
    </ol>
    <p><b>Uzávěrka podání kandidátek je zákonem stanovena na 66 dnů před volbami do 16 hodin.</b></p>
    <p>Registrační úřad přezkoumá ve lhůtě od 60 do 66 dnů přede dnem voleb do zastupitelstva obce předložené kandidátní listiny. <b>Aby byla listina bez komplikací přijata a nebyla navrácena k opravě, musí obsahovat následující</b>:</p>
    <ul>
      <li>název zastupitelstva obce,</li>
      <li>označení volebního obvodu, jsou-li volební obvody vytvořeny,</li>
      <li>název volební strany a označení, o jaký typ volební strany jde, s uvedením názvu politických stran a politických hnutí, </li>
      <li>jména a příjmení kandidátů, jejich věk a povolání, část obce; nečlení-li se obec na části, obec; kde jsou přihlášeni k trvalému pobytu, název politické strany nebo politického hnutí, jehož jsou členy, nebo údaj, že nejsou členy žádné politické strany nebo politického hnutí, </li>
      <li>pořadí na kandidátní listině vyjádřené pomocí arabského čísla,</li>
      <li>jméno a příjmení zmocněnce volební strany a jeho náhradníka s uvedením místa, kde jsou přihlášeni k trvalému pobytu, není-li volební stranou nezávislý kandidát, </li>
      <li>jde-li o koalici, název politické strany nebo politického hnutí, které kandidáta navrhlo,</li>
      <li>jde-li o sdružení politických stran nebo politických hnutí a nezávislých kandidátů, označení politické strany nebo politického hnutí, které kandidáta navrhlo, nebo označení, že jde o nezávislého kandidáta,</li>
      <li>podpis zmocněnce volební strany; u nezávislého kandidáta podpis kandidáta; podává-li kandidátní listinu politická strana, politické hnutí nebo jejich koalice anebo sdružení politických stran nebo politických hnutí a nezávislých kandidátů, jméno a příjmení, označení funkce a podpis osoby oprávněné jednat jejich jménem, popřípadě jménem organizační jednotky, je-li ustavena.</li>
    </ul>
    <h3 className="tab__subtitle">Jak zvládnout kampaň</h3>
    <p>Ze všeho nejdříve je třeba připravit si <b>strategii</b>, která by měla zodpovědět na základní otázky kampaně: Komu, co, jakým způsobem a kdy a kde budu říkat, abych jej přesvědčil natolik, že přijde zvolit naši stranu.</p>
    <p>
      Celou kampaň si rozfázuj nejlépe na tři etapy:
      <ol>
        <li>
          <p><b>Jsme tady a kandidujeme</b></p>
          <ul>
            <li>v první fázi kampaně pouze dejte vašim spoluobčanům najevo, že jste tady a kandidujete. Postačí rozhovor v novinách, pár reklamních ploch na hlavním náměstí a letáčky rozdané na nádraží.</li>
          </ul>
        </li>
        <li>
          <p><b>Vyřešíme problémy v naší obci</b></p>
          <ul>
            <li>Ve druhé fázi je potřeba zaujmout stanoviska ke konkrétním politickým tématům ve vaší obci, vyjádřit je v podobě volebního programu, témat, hesel a sloganů.</li>
          </ul>
        </li>
        <li>
          <p><b>Pojďte nás volit</b></p>
          <ul>
            <li>Poslední týden či dva před volbami potřebují voliči ještě popostrčit k tomu, aby vás skutečně šli volit. Vyzvěte je k tomu. Burcujte. Buďte dramatičtí a působte na emoce.</li>
          </ul>
        </li>
      </ol>
    </p>
    <p><b>Doporučujeme věnovat velkou pozornost kontaktní kampani. Jelikož přímý kontakt s voliči je nenahraditelný.</b></p>
    <p>
      Při kontaktní kampani se neobejdeš bez:
      <ul>
        <li><b>Kandidátů</b> – ti svojí účastí dávají najevo, že se o voliče zajímají a chtějí si s nimi povídat. Při kontaktní kampani lidi nejvíce lákají známější kandidáti, ale kontaktní kampaň je také ideálním způsobem, jak voličům představit a přiblížit i méně známé tváře. Kandidáty mohou nahradit i dobrovolníci, ale pamatujte, že kandidát působí na voliče daleko lépe.</li>
        <li><b>Dobrovolníků</b> – ti jsou druhou případně třetí (po významných podporovatelích) nejdůležitější postavou v kontaktní kampani. Reprezentují stranu a komunikují jejím jménem. Určitým způsobem suplují politika, ale nikdy se nejedná o plnohodnotné zastoupení.</li>
        <li><b>Letáků s heslem, základními programovými tezemi.</b></li>
      </ul>
    </p>
    <p>
      Určete si zodpovědnou osobu, která bude celou akcii koordinovat a bude znát všechny informace. Ideálně by to neměl být sám kandidát.<br />
      Nezapomeňte všechny účastníky dopředu informovat o každé akci a ideálně je i vyškolit. Dobrovolníci musí znát vaše základní teze, kdo jste a proč kandidujete.
    </p>
    <p>Mezi hlavní typy kontaktní kampaně patří door-to-door setkávání, pořádání různých akcí, propagace na veřejných událostech, rozdávání letáků atd… </p>
    <p><i className="tab__abstract">To jest to nejdůležitější, co musíš vědět pro začátek. Jestli chceš tipy, jak se dostat do médií, jak vytvořit vlastní volební plakát či jak sehnat tolik potřebné peníze na kampaň, pročti si celou příručku <a style={{color: 'blue'}} href="http://files.lepsikladno.webnode.cz/200000098-3eb1f3fe6e/jak-vyhrat-volby14.pdf" target="_blank">„Jak vyhrát komunální volby“</a> Institutu politického marketingu.</i></p>
    <p><i className="tab__abstract">Navíc doporučujeme prostudovat zákon o volbách do zastupitelstev obcí, ve kterém se dozvíš všechny technické detaily.</i></p>
  </article>,

  '#po-volbach': <article className="tab">
    <h2 className="tab__title">Po volbách</h2>
    <i className="tab__abstract">Možná právě přemýšlíš, co bude náplní tvé práce, až se staneš členem zastupitelstva. Možná hledáš řešení problémů, které trápí tvoji obec. Na následujících řádcích se Ti proto pokusíme předat pár tipů a rad, ačkoliv neexistuje univerzální návod na to, jak být dobrým zastupitelem.</i>
    <p>Nejprve musíš mít vizi. Tedy představu, s čím chceš do voleb jít a co by bylo záhodno na své obci změnit. Samozřejmě nemusíš hned přijít s koncepcí rozvoje na 50 let dopředu, ale i drobná, a hlavně prospěšná řešení menších záležitostí jsou velmi cenná.</p>
    <p>Tuto vizi, kterou následně rozpracovanou v „program“ využiješ už před volbami ve své kampani, ale nezapomeň, že po úspěšných volbách ho musíš taktéž naplňovat. Také proto Tě voliči zvolili do funkce. Pokud tedy chceš, aby ve tvé obci vzniklo dopravní hřiště, bezpečnější přechod nebo lavičky v parku, je nutné si za tím stát. Můžeš chtít třeba změnit i strategické směřování Tvé obce. Zdá se Ti, že dotuje jen sport a nedává peníze na kulturu? Pak je třeba na to upozorňovat, toto téma zahrnout do jednání a snažit se prosadit návrhy, které by to změnily.</p>
    <h3 className="tab__subtitle">Jakým způsobem jít co nejrychleji naproti realizaci svého předvolebního programu?</h3>
    <p>Jednou z cest, kterou se můžeš vydat, je <b>být členem některé z komisí, výborů či zvláštního orgánu</b>. Skrze ně se můžeš věnovat agendě, která se zaobírá pouze tématem, kterému se chceš věnovat. Místo v takovémto orgánu je však otázkou Tvého volebního výsledku, pokud například kandiduješ za stranu, která vyhraje, máš křeslo skoro jisté ☺ .</p>
    <p>Malé obce většinou zřizují pouze <b>povinný finanční a kontrolní výbor</b>, případně výbor pro národnostní menšiny, komise nezřizují vůbec. Vše se řeší v několikačlenném zastupitelstvu a o administrativních záležitostech rozhoduje starosta s místostarostou. Ve větších městech se ale právě v komisích a výborech „předjednávají“ nejrůznější prodeje, investiční záměry, které pak slouží jako doporučující materiál zastupitelstvu. A právě v nich zasedá řada zastupitelů a někteří dokonce v několika naráz.</p>

    <p>A teď nějaké tipy a rady:</p>
    <h3 className="tab__subtitle">1) Financování projektů</h3>
    <ol type="a">
      <li>
        <p><b>dotace</b></p>
        <p>Pokud je Tvým cílem v obci něco postavit, předělat či uspořádat nějakou akci, těžko takový cíl naplníš bez finančních prostředků. Rozpočet samozřejmě není neomezený, takže většina obcí žádá o nejrůznější dotace. Nabízíme Ti seznam užitečných webových portálů, na kterých se dozvíš, mnoho podstatných informací.</p>
        <p>
        Určitě zkus fondy EU, najdeš tam mnoho programů, které by mohly sedět tvým plánům:<br />
        <ul>
          <li><a href="http://www.dotacni.info/dotace-podle-oboru/mesta-obce-kraje/" target="_blank">
            http://www.dotacni.info/dotace-podle-oboru/mesta-obce-kraje/
          </a></li>
          <li><a href="http://www.europroject.cz/aktualni-dotacni-prilezitosti/aktualni-dotacni-moznosti-obce/" target="_blank">
            http://www.europroject.cz/aktualni-dotacni-prilezitosti/aktualni-dotacni-moznosti-obce/
          </a></li>
          <li><a href="https://www.dotace-info.cz/" target="_blank">
            https://www.dotace-info.cz/
          </a></li>
        </ul>
        </p>
      </li>
      <li>
        <p><b>granty pro podporu komunit</b></p>
        <p>
        Některé nadace vypisují granty s cílem podpořit komunity. O takový grant můžeš z vlastní iniciativy zažádat i Ty a jakožto zastupitel pak uspořádat nějaký projekt či akci.<br />
        <ul>
          <li><a href="http://www.nadacevia.cz/" target="_blank">
            http://www.nadacevia.cz/
          </a></li>
          <li><a href="https://www.komunitninadace.cz/" target="_blank">
            https://www.komunitninadace.cz/
          </a></li>
          <li><a href="http://www.konabo.cz/6675/asociace-komunitnich-nadaci/" target="_blank">
            http://www.konabo.cz/6675/asociace-komunitnich-nadaci/
          </a></li>
        </ul>
        </p>
      </li>
    </ol>
    <h3 className="tab__subtitle">2) Otevřenost</h3>
    <p>Pamatuj si, že dobrý zastupitel je otevřený vůči svým spoluobčanům. Musíš jim svoji práci přiblížit, ukázat, na čem pracuješ, co jste v zastupitelstvu dokázali a proč to děláte. K tomu skvěle poslouží nějaké setkání zastupitelů s veřejností.  Můžete jej naplánovat formou pikniku, setkání v kavárně či uspořádat den otevřených dveří v budově zastupitelstva. Osobním setkáním a diskuzí s lidmi poznáš, co by si přáli a na čem byste mohli zapracovat</p>
    <p>Zároveň Ti doporučujeme být aktivním i na Facebooku, Instagramu či Twitteru, je to další krok k přiblížení se lidem a projevování Tvých názorů. Pro ty pak budeš čitelnější a můžeš tak motivovat ostatní lidi k zapojení se do zlepšování vaší obce. Zároveň můžeš na sociálních sítích informovat o tom, co právě děláte.</p>
    <h3 className="tab__subtitle">3) Komunikace s médii</h3>
    <p>Zejména pokud budeš zastupitelem ve větším městě, může o tvé práci psát například regionální deník. Je dobré proto s médii a novináři komunikovat a vyjadřovat se k tématům, které považuješ za důležitá a která zajímají spoluobčany. K lidem se tak dostane i tvůj úhel pohledu. Lidé budou mít Tvé jméno spojené s určitými názory, což Ti může pomoct při dalších volbách.</p>
    <h3 className="tab__subtitle">4) Připravenost a pravidelná docházka</h3>
    <p>Co se praxe týče, zastupitelstvo se schází většinou jednou za měsíc, Tvým úkolem je přečíst si projednávané materiály, dostavit se a hlasovat. Velmi doporučujeme si materiály důkladně nastudovat, abys věděl, o čem se bude jednat. Klidně si doma připrav i nějaké argumenty, které podpoří Tvůj cíl. Účast není povinná, ale co si budeme nalhávat, jestli chceš dělat práci zastupitele dobře, je fajn být poctivý a docházet pravidelně.</p>
    <p>Správný člen zastupitelstva by měl být též obeznámen s nejnovějšími právními předpisy, které se týkají obcí. Samozřejmě nemusíš každý den sledovat, co je nového ve Sbírce zákonů, ale pomůže Ti např. tato webová stránka: <a href="https://www.obecniportal.cz/" target="_blank">https://www.obecniportal.cz/</a>. Mimochodem zde nalezneš mnoho dalších informací, které by se Ti mohli při práci zastupitele hodit.</p>
    <h3 className="tab__subtitle">5) Poctivost a zodpovědnost</h3>
    <p>Maminky nám vštěpovaly, že s poctivostí nejdál dojdeš, a to platí i v tomto případě a dokonce dvojnásobně! Když zastupuješ lidi v obci, jednej především v jejich zájmu. A jak říká klasik: "Neber úplatky, neber úplatky, nebo se z toho zblázníš!"</p>
    <br /><br />
    <p><i className="tab__abstract">Pokud hledáš něco konkrétnějšího, zkus zhlédnout na tomto webu pár videí a pročíst si rozhovory se zkušenými politiky. Jestliže hledáš ještě víc, pak právě pro tebe chystáme letní intenzivní workshop s politiky, akademiky a odborníky, jehož hlavní téma je „jak být dobrým zastupitelem“ (více info na stránce  <Link to="/workshop">Workshop</Link>).</i></p>
    <p><i className="tab__abstract">Mimo tyto naše aktivity, na internetu najdeš nepřeberné množství inspirace a konkrétních způsobů řešení komunitních problémů</i></p>
  </article>,
}