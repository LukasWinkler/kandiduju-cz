import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Section, Button, ArticleItem } from '../'
import './ArticlesSection.css'

class ArticlesSection extends React.Component {
  render() {
    const {articles} = this.props
    const articlesToRender = Object.values(articles)
    if (!articles) return null
    return (
      <Section title='Články & Aktuality' type="grey" >
        <div className="articles-section">
          <div className="articles-section__articles">
            {articlesToRender.map(article => <ArticleItem article={article} />) }
          </div>
          <div className="articles-section__buttons">
            <Button
              text="Všechny články"
              color="orange"
              link="/clanky"
            />
          </div>
        </div>
      </Section>
    )
  }
}

export default (ArticlesSection)
