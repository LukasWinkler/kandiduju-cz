import React from 'react'
import { compose } from 'recompose'
import { connect } from 'react-redux'
import { Button } from './'
import './ContactUsForm.css'

class ContactUsForm extends React.Component {
  constructor() {
    super()
    this.state = {
      email: '',
      subject: '',
      message: '',
    }
    this.handleOnChange = this.handleOnChange.bind(this)
  }

  handleOnChange(field, value) {
    if (field === 'email') this.setState({ email: value })
    if (field === 'subject') this.setState({ subject: value })
    if (field === 'message') this.setState({ message: value })
  }

  render() {
    const { handleOnChange } = this
    const { email, subject, message } = this.state
    return (
      <form className="contact-us-form">
        <div className="form-item">
          <div className="form-item__label required">
            Váš email
          </div>
          <div className="form-item__field">
            <input
              name="email"
              placeholder='budouci.zastupitel@vasedomena.cz'
              value={email}
              onChange={(e) => handleOnChange('email', e.target.value)}
            />
          </div>
        </div>
        <div className="form-item">
          <div className="form-item__label">
            Předmět
          </div>
          <div className="form-item__field">
            <input
              name="subject"
              placeholder='Předmět Vaší zprávy?'
              value={subject}
              onChange={(e) => handleOnChange('subject', e.target.value)}
            />
          </div>
        </div>
        <div className="form-item">
          <div className="form-item__label required">
            Vaše zpráva
          </div>
          <div className="form-item__field">
            <textarea
              name="message"
              placeholder='Co máte na srdci...?'
              rows="15"
              value={message}
              onChange={(e) => handleOnChange('message', e.target.value)}
            />
          </div>
        </div>
        <Button text="Odeslat" />
      </form>
    )
  }
}

export default (ContactUsForm)