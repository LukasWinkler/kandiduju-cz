import React from 'react'
import { Link } from 'react-router-dom'
import logo from '../images/logo-classic.svg'
import chevron from '../images/chevron.svg'
import './Footer.css'

const scrollTop = () => {
  document.getElementById('root').scrollIntoView({ behavior: 'smooth', block: 'start' })
}

const Footer = () => (
  <div className="page-footer">
    <div className="page-footer__content container">
      <div className="up-button" onClick={() => scrollTop()}>
        <img src={chevron}/>
      </div>
      <div className="contact">
        <div className="contact__title">Kontaktujte nás</div>
        <p>kandiduju@mladiobcane.cz</p>
        <p>+420 722 983 153</p>
      </div>
      <div className="logo-copyright">
        <p><img src={logo} alt="Kandiduju.cz" /></p>

        <p>&copy; {(new Date()).getFullYear()} Kandiduju.cz, všechna práva vyhrazena</p>
      </div>
      <div className="social-author">
        <div className="social">
          <div className="social__title">
            Sledujte nás na Facebooku
          </div>
          <div className="social__icons">
            <span><a href="https://www.facebook.com/Kandidujucz-977270829117287/" target="_blank" class="icon-10 facebook" title="Facebook"><svg viewBox="0 0 512 512"><path d="M211.9 197.4h-36.7v59.9h36.7V433.1h70.5V256.5h49.2l5.2-59.1h-54.4c0 0 0-22.1 0-33.7 0-13.9 2.8-19.5 16.3-19.5 10.9 0 38.2 0 38.2 0V82.9c0 0-40.2 0-48.8 0 -52.5 0-76.1 23.1-76.1 67.3C211.9 188.8 211.9 197.4 211.9 197.4z"/></svg></a></span>
          </div>
        </div>
        <div className="author">
          <p>S úsměvem vyrobil <b>Lukáš Winkler</b></p>
        </div>
      </div>
      <div className="author-2">
        <p>S úsměvem vyrobil <b>Lukáš Winkler</b></p>
      </div>
    </div>
  </div>
)

export default (Footer)
