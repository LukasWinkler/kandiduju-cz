import React from 'react'
// import { injectIntl, defineMessages } from 'react-intl'
import './CookiesInfo.css'

function getCookie(cname) {
  const name = `${cname}=`
  const decodedCookie = decodeURIComponent(document.cookie)
  const ca = decodedCookie.split(';')
  for (let i = 0; i < ca.length; i += 1) {
    let c = ca[i]
    while (c.charAt(0) === ' ') {
      c = c.substring(1)
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length)
    }
  }
  return ''
}

class CookiesInfo extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isCookiesConfirmed: true,
    }
  }
  componentDidMount() {
    const isCookiesConfirmed = getCookie('cookiesConfirmed')
    this.setState({ isCookiesConfirmed }) // eslint-disable-line
  }
  shouldComponentUpdate(newState) {
    const { isCookiesConfirmed } = this.state
    return newState.isCookiesConfirmed !== isCookiesConfirmed
  }
  confirmCookie() {
    document.cookie = 'cookiesConfirmed=true; path=/; expires=Thu, 01 Jan 2030 00:00:00 UTC'
    this.setState({ isCookiesConfirmed: true })
  }
  render() {
    const hide = this.state.isCookiesConfirmed
    const classNames = [
      'cookies-info',
    ]
    if (!hide) classNames.push('show')
    return (
      <div className={classNames.join(' ')}>
        <div className="content">
          Sdělujeme Vám, že tento web využívá cookies.
          <button onClick={() => this.confirmCookie()}>
            Souhlasím
          </button>
        </div>
      </div>
    )
  }
}

export default CookiesInfo

// const messages = defineMessages({
//   text: {
//     id: 'cookiesInfo.text',
//     defaultMessage: 'Aby bylo legislativě EU učiněno zadost, sdělujeme, že tento web využívá cookies. Pro jistotu taky prohlašujeme, že neobsahuje žádné alergeny.',
//   },
//   buttonText: {
//     id: 'cookiesInfo.buttonText',
//     defaultMessage: 'Souhlasím',
//   },
// })

/* <span>'Aby bylo legislativě EU učiněno zadost, sdělujeme, že tento web využívá cookies. Pro jistotu taky prohlašujeme, že neobsahuje žádné alergeny.'</span>

<p><br>Co je to cookie?</br></p>
<p>Cookies jsou malé textové soubory, které internetové stránky ukládají při Vaší návštěvě do Vašeho počítače. Cookies se běžně používají k zajištění efektivnějšího chodu internetových stránek, poskytovaných služeb a funkcí pro uživatele.</p>
<p>Jsou dva druhy cookies, trvalé (persistent cookies), nebo dočasné (session cookies). Trvalý cookie je textový soubor přenášený serverem prohlížeči, který po jeho uložení prohlížečem zůstává aktivní až do jeho dedikovaného data odstranění (neodstraní-li uživatelem odstraněn dříve). Oproti tomu, dočasný cookie je odstraněn po zavření prohlížeče.</p>
<p>Cookies neobsahují žádné informace, na základě kterých bychom Vás mohli přímo identifikovat.</p>

<p>Jaké cookies využíváme a proč?</p>
<p>Některé cookies jsou nezbytné pro zajištění řádného fungovaní našich stránek. Pokud v nastavení ve svém prohlížeči takové cookies internetových zakážete, může to ovlivnit správné fungování našich stránek a služeb námi poskytovaných. V následující tabulce shrnujeme cookies, které používáme, které používáme a jejich funkci:</p> */
/* <p>[prosíme o doplnění názvu jednotlivého cookies, funkce a doby platnosti/používání cookies]</p> TODO */
/* <p>Využíváme rovněž cookies poskytovaných třetími stranami, které slouží k získávání informací o tom, jak využíváte naše stránky.</p>
<p>Jedná se o následující cookies:</p> */
/* <p>[prosíme o doplnění názvu jednotlivého cookies, funkce a doby platnosti/používání cookies]</p> TODO*/
/* <p>Více informací o pravidlech využívání cookies společností Google naleznete na internetových stránkách: <a href="https://www.google.com/policies/technologies/cookies/">https://www.google.com/policies/technologies/cookies/</a></p>
<p><br>Jak změnit nastavení souborů cookies?</br></p>
<p>Ve většině internetových prohlížečů lze zásady používání cookies spravovat v nastavení prohlížeče.</p>
<p>Pokud chcete změnit nastavení souborů cookie nainstalované službou Google Analytics, navštivte stránku <a href="http://tools.google.com/dlpage/gaoptout">http://tools.google.com/dlpage/gaoptout</a>.</p>
<p><br>Neukojili jsme Vaši zvědavost?</br></p>
<p>Máte-li jakékoli dotazy týkající se našich souborů cookie nebo zde uvedených zásad používání souborů cookie, kontaktujte nás prosím e-mailem na adrese: hello@virtii.com</p> */
