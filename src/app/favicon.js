/* eslint-disable comma-dangle, quote-props, quotes */
export default {
  "link": [
    {
      "rel": "apple-touch-icon",
      "sizes": "180x180",
      "href": "/favicons/apple-touch-icon.png"
    },
    {
      "rel": "icon",
      "type": "image/png",
      "sizes": "32x32",
      "href": "/favicons/favicon-32x32.png"
    },
    {
      "rel": "icon",
      "type": "image/png",
      "sizes": "16x16",
      "href": "/favicons/favicon-16x16.png"
    },
    {
      "rel": "manifest",
      "href": "/favicons/manifest.json"
    },
    {
      "rel": "mask-icon",
      "href": "/favicons/safari-pinned-tab.svg",
      "color": "#cbb88a"
    },
    {
      "rel": "shortcut icon",
      "href": "/favicons/favicon.ico"
    }
  ],
  "meta": [
    {
      "name": "msapplication-TileColor",
      "content": "#ffffff"
    },
    {
      "name": "msapplication-TileImage",
      "content": "/favicons/mstile-144x144.png"
    },
    {
      "name": "msapplication-config",
      "content": "/favicons/browserconfig.xml"
    },
    {
      "name": "theme-color",
      "content": "#ffffff"
    }
  ]
};
