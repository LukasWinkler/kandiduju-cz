import React from 'react'
import PropTypes from 'prop-types'

const Col = ({ children, ...props }) => {
  const classes = []
  if (props.className) classes.push(props.className)
  if (props.xs) classes.push(`col-xs-${props.xs}`)
  if (props.sm) classes.push(`col-sm-${props.sm}`)
  if (props.md) classes.push(`col-md-${props.md}`)
  if (props.lg) classes.push(`col-lg-${props.lg}`)
  if (props.xsOffset) classes.push(`col-xs-offset-${props.xsOffset}`)
  if (props.smOffset) classes.push(`col-sm-offset-${props.smOffset}`)
  if (props.mdOffset) classes.push(`col-md-offset-${props.mdOffset}`)
  if (props.lgOffset) classes.push(`col-lg-offset-${props.lgOffset}`)
  if (props.xsPull) classes.push(`col-xs-pull-${props.xsPull}`)
  if (props.smPull) classes.push(`col-sm-pull-${props.smPull}`)
  if (props.mdPull) classes.push(`col-md-pull-${props.mdPull}`)
  if (props.lgPull) classes.push(`col-lg-pull-${props.lgPull}`)
  if (props.xsPush) classes.push(`col-xs-push-${props.xsPush}`)
  if (props.smPush) classes.push(`col-sm-push-${props.smPush}`)
  if (props.mdPush) classes.push(`col-md-push-${props.mdPush}`)
  if (props.lgPush) classes.push(`col-lg-push-${props.lgPush}`)
  return <div className={classes.join(' ')}>{children}</div>
}

Col.defaultProps = {
  children: null,
  xs: undefined,
  sm: undefined,
  md: undefined,
  lg: undefined,
  xsOffset: undefined,
  smOffset: undefined,
  mdOffset: undefined,
  lgOffset: undefined,
  xsPull: undefined,
  smPull: undefined,
  mdPull: undefined,
  lgPull: undefined,
  xsPush: undefined,
  smPush: undefined,
  mdPush: undefined,
  lgPush: undefined,
  className: '',
}

Col.propTypes = {
  children: PropTypes.any,
  xs: PropTypes.number,
  sm: PropTypes.number,
  md: PropTypes.number,
  lg: PropTypes.number,
  xsOffset: PropTypes.number,
  smOffset: PropTypes.number,
  mdOffset: PropTypes.number,
  lgOffset: PropTypes.number,
  xsPull: PropTypes.number,
  smPull: PropTypes.number,
  mdPull: PropTypes.number,
  lgPull: PropTypes.number,
  xsPush: PropTypes.number,
  smPush: PropTypes.number,
  mdPush: PropTypes.number,
  lgPush: PropTypes.number,
  className: PropTypes.string,
}

export default Col
