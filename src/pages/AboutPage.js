import React from 'react'
import { PageWrapper } from './'
import './AboutPage.css'
import logo from '../images/logo_transparent_crop.png'
import {landImgs} from '../config'

class AboutPage extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0)
    document.title = "O projektu | Kandiduju.cz"
  }

  render() {
    return (
      <PageWrapper title="O projektu" image={landImgs.land1}>
        <div className="about-page container">
          <div className="about-page__about">
            <p>Kampaň Kandiduju.cz vznikla za účelem podpory zapojení mladých lidí v komunální politice. S blížícími se podzimními komunálními volbami 2018 chceme upozornit na dosavadní takřka mizivou míru angažovanosti mladých v obecních zastupitelstvech. Navzdory tomuto faktu ve svém okolí pozorujeme trend rostoucího zájmu mladých angažovat se ve svých městech a obcích, kandidovat do zastupitelstev a přispět tak svou měrou ke zlepšení poměrů ve svém nejbližším okolí.</p>
            <p>Pro nedostatek informací a jiné formy podpory však často vůbec nedojde k činům nebo dotyční kandidaturu zkusí, ale šance mají nízké. Kromě toho, dobrá vůle také není všespásná a samotný zisk mandátu zastupitele mnoho neznamená, neví-li člověk, jak s touto funkcí naložit.</p>
            <p>Chceme pomoci odstranit tyto bariéry dělící odhodlání něco změnit od skutečné smysluplné participace a mladé lidi, kteří o kandidatuře uvažují, vhodně podpořit. Mládí nevnímáme jako předpoklad, ale nemělo by být překážkou.</p>
            <p>Na těchto stránkách a facebooku projektu najdeš informace nezbytné k základnímu porozumění fungování komunálu, tipy a rady k volbám i do začátku působení na radnici a především rozhovory se zkušenými politiky a osobnostmi napříč regiony a politickým spektrem.</p>
            <h2>A kdo za projektem stojí?</h2>
            <p>Mladí občané jsou již 4. rokem fungující zapsaný spolek čítající více než 50 členů převážně z řad vysokoškoláků. Naším cílem je zvyšování politické gramotnosti a zájmu mladých o veřejný prostor. Ačkoliv máme ve svých řadách aktivní politiky, nespojuje nás žádná politická doktrína, nýbrž víra v to, že ke správnému vývoji společnosti je nezbytné aktivní občanství široké veřejnosti. Snažíme se měnit rezignovaný až netečný pohled části společnosti (a především mladých) na politický systém, a svým vzorem a vzdělávacími aktivitami podněcovat zájem lidí z našeho okolí stát se aktivními občany.</p>
          </div>
          <div className="about-page__logo">
            <a href="http://mladiobcane.cz/" target="_blank" rel="noopener noreferer">
              <img src={logo} alt="Mladí Občané" />
            </a>
          </div>
        </div>
      </PageWrapper>
    )
  }
}

export default (AboutPage)
