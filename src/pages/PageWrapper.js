import React from 'react'
import PropTypes from 'prop-types'

import './PageWrapper.css'

class PageWrapper extends React.PureComponent {
  render() {
    const { image, title } = this.props
    return (
      <div className="page-wrapper">
        <div className="page-header">
          <img src={image} />
        </div>
        <div className="page-main">
          <div className="page-main__title">
            <h1>
              {title}
            </h1>
          </div>
          <div className="page-main__children">
            {this.props.children}
          </div>
        </div>
      </div>
    )
  }
}

PageWrapper.defaultProps = {
  title: 'Page Title',
  image: 'https://placeimg.com/2400/400/nature',
}

PageWrapper.propTypes = {
  title: PropTypes.string,
  image: PropTypes.string,
  children: PropTypes.node.isRequired,
}

export default (PageWrapper)
