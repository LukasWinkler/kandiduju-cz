import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import './Button.css'

const Button = ({ color, onClick, link, text, ...rest }) => (
  <div className={`button button--${color}`} onClick={onClick} {...rest}>
    {link
      ? <Link to={link}><span>{text}</span></Link>
      : <span>{text}</span>
    }
  </div>
)
Button.defaultProps = {
  color: 'orange',
  onClick: null,
  text: 'Button text',
  link: '',
}

Button.propTypes = {
  color: PropTypes.string,
  onClick: PropTypes.function,
  text: PropTypes.string,
  link: PropTypes.string,
}

export default Button
