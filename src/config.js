
const landImgs = {
  land1: 'https://res.cloudinary.com/farlanis/image/upload/v1525593591/land-b-min.png',
  land2: 'https://res.cloudinary.com/farlanis/image/upload/v1525593589/land-d-min.png',
  land3: 'https://res.cloudinary.com/farlanis/image/upload/v1525593588/land-a-min.png',
  land4: 'https://res.cloudinary.com/farlanis/image/upload/v1525593581/land-f-min.png',
  land5: 'https://res.cloudinary.com/farlanis/image/upload/v1525593570/land-c-min.png',
}

export {
  landImgs
}