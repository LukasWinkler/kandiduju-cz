import React from 'react'
import PropTypes from 'prop-types'
import { SectionTitle } from './'

import './Section.css'

const Section = ({ type, title, children, secondaryTitle }) => (
  <div className={`section section--${type}`}>
    {title
      ? <SectionTitle title={title} />
      : null
    }
    {secondaryTitle
      ? <div className="secondary-title">{secondaryTitle}</div>
      : null
    }
    <div className="section__content container">
      {children}
    </div>
  </div>
)

Section.defaultProps = {
  type: 'white',
  children: null,
  title: '',
  secondaryTitle: '',
}

Section.propTypes = {
  type: PropTypes.string,
  title: PropTypes.string,
  secondaryTitle: PropTypes.string,
  children: PropTypes.element,
}

export default (Section)
