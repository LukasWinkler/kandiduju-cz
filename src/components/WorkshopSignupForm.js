import React from 'react'
import { Upload, Icon, Button as AntdButton} from 'antd'
import { Button } from './'
import './WorkshopSignupForm.css'
import firebaseConf from '../firebase'
//import { objectToArray } from '../helpers'

class WorkshopSignupForm extends React.Component {
  constructor() {
    super()
    this.state = {
      name: '',
      email: '',
      age: 18,
      gdpr: false,
      party: '',
      city: '',
      phone: '',
      file: '',
    }    
  }

  handleOnChange = (field, value) => {
    const editedField = {}
    editedField[field] = value
    this.setState(editedField)
    // if (field === 'name') this.setState({ name: value })
    // if (field === 'email') this.setState({ email: value })
    // if (field === 'age') this.setState({ age: value })
    // if (field === 'gdpr') this.setState({ gdpr: value })
    // if (field === 'party') this.setState({ party: value })
    // if (field === 'city') this.setState({ city: value })
    // if (field === 'phone') this.setState({ phone: value })
    // const fb = firebase.database().ref('registration').once('value').then(snapshot => {this.getValues(objectToArray(snapshot.val()))})     
  }

  handleSubmit = (e) => {
    e.preventDefault()
    console.log('submit')
    console.log('this.state', this.state)
    const params = Object.assign({}, {...this.state})
    firebaseConf.database().ref('registration').push(params)
      .then(() => {
        console.log('success', 'Your message was sent successfull');
      }).catch(() => {
        console.log('danger', 'Your message could not be sent');
      });
  }

  handleFileUpload = () => {
    this.setState({uploading: true})
    firebaseConf.storage().ref().child('registrations/file')
      .put(this.state.file)
      .then(snap => {
        this.setState({ uploading: false, fileUploaded: true })
        console.log('sent');
        // the loading percent logic here?
        // how do i keep tabs on the percent?
      })
      .catch(err => this.setState({error: err.message}))
  }

  render() {
    const { handleOnChange, handleSubmit } = this
    const { name, email, age, gdpr, party, city, phone } = this.state
    return (
      <form className="workshop-signup-form" onSubmit={e => handleSubmit(e)}>
        <div className="form-item">
          <div className="form-item__label required">
            Váše jméno
          </div>
          <div className="form-item__field">
            <input
              name="name"
              placeholder='Antonín Novák'
              value={name}
              onChange={(e) => handleOnChange('name', e.target.value)}
              required
            />
          </div>
        </div>

        <div className="form-item">
          <div className="form-item__label required">
            Váš věk
          </div>
          <div className="form-item__field">
            <input
              name="age"
              type="number"
              min="18"
              max="100"
              step="1"
              placeholder='Váš věk'
              value={age}
              onChange={(e) => handleOnChange('age', e.target.value)}
              required
            />
          </div>
        </div>

        <div className="form-item">
          <div className="form-item__label required">
            Váš email
          </div>
          <div className="form-item__field">
            <input
              name="email"
              placeholder='budouci.zastupitel@vasedomena.cz'
              value={email}
              onChange={(e) => handleOnChange('email', e.target.value)}
              required
            />
          </div>
        </div>

        <div className="form-item">
          <div className="form-item__label required">
            Vaše telefonní číslo
          </div>
          <div className="form-item__field">
            <input
              name="phone"
              placeholder='+420 112 293 113'
              value={phone}
              onChange={(e) => handleOnChange('phone', e.target.value)}
              required
            />
          </div>
        </div>

        <div className="form-item">
          <div className="form-item__label required">
            Jméno Vaší obce
          </div>
          <div className="form-item__field">
            <input
              name="city"
              value={city}
              placeholder='Horní-Dolní'
              onChange={(e) => handleOnChange('city', e.target.value)}
              required
            />
          </div>
        </div>

        <div className="form-item">
          <div className="form-item__label required">
            Jméno Vaší strany/hnutí
          </div>
          <div className="form-item__field">
            <input
              name="party"
              value={party}
              placeholder='Hnutí Horní-Dolní'
              onChange={(e) => handleOnChange('party', e.target.value)}
              required
            />
          </div>
        </div>

        <div className="form-item">
          <div className="form-item__label required">
            Váš program/idea
          </div>
          <div className="form-item__field">
            {/* <input
              className="inputfile"
              name="file"
              type="file"
              onChange={e => this.setState({ file: e.target.files[0]})}            
            />             */}
            <Upload beforeUpload={(file, fileList) => {this.setState({ file: file}); console.log(file, fileList)}} onChange={(a, b, c) => console.log(a, b, c)}>
              <AntdButton>
                <Icon type="upload" /> Vybrat soubor
              </AntdButton>
            </Upload>
          </div>
          
          {this.state.fileUploaded && <div style={{ color: '#4BB543'}}>Soubor byl nahrán</div>}
          <Button color="inverted" onClick={this.handleFileUpload} text="Nahrát soubor" style={{ maxWidth: '16rem', textAlign: 'center'}}/> 
        </div>
        
          
        <div className="form-item">
          <input
            type="checkbox"
            name="gdpr"
            value={gdpr}
            onChange={(e) => handleOnChange('gdpr', e.target.value)}
            required
            accept="application/pdf"
          />
          <label htmlFor="gdpr">Souhlasím s podmínkami registrace</label>
        </div>
        <input type="submit" value="submit" />
        <Button text="Přihlásit se" type="submit"/>
      </form>
    )
  }
}


export default (WorkshopSignupForm)