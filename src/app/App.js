import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import debounce from 'debounce'
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom'
import { CookiesInfo, Footer, Header } from '../components'
import * as pages from '../pages'
import './App.css'
import './grid.css'

import meta from './meta'

class App extends React.Component {
  state = {
    values: []
  }

  getValues = (values) => {
    this.setState({ values })
  }
  componentDidCatch(error, info) {
    console.log(info)
    console.log(error)
  }
  render() {
    console.log('this.state.values', this.state.values)
    return (
      <Router>
        <div className="app-container">
          <Helmet
            htmlAttributes={{ lang: 'cs' }}
            titleTemplate={meta.title}
            meta={meta.metas}
            link={meta.links}
          />
          <Header />
          <Switch>
            <Route exact path='/' component={pages.LandingPage} />
            <Route exact path='/o-projektu' component={pages.AboutPage} />
            <Route exact path='/clanky/:item' component={pages.ArticleDetailPage} />
            <Route exact path='/clanky' component={pages.ArticlesPage} />
            <Route exact path='/workshop' component={pages.WorkshopsPage} />
            <Route exact path='/informace' component={pages.InformationPage} />
            <Route exact path='/login' component={pages.LoginPage} />
            <Route component={pages.NotFoundPage} />
          </Switch>
          <CookiesInfo />
          <Footer />
        </div>
      </Router>
    )
  }
}

export default (App)


const routesMap = {
  landing: {
    pattern: '/',
    exactly: true,
    component: pages.LandingPage
  },
  about : {
    pattern: '/o-projektu',
    exactly: true,
    component: pages.AboutPage
  },
  articleDetail: {
    pattern: '/clanky/:item',
    exactly: true,
    component: pages.ArticleDetailPage
  },
  articles: {
    pattern: '/clanky',
    exactly: true,
    component: pages.ArticlesPage
  },
  workshops: {
    pattern: '/workshopy',
    exactly: true,
    component: pages.WorkshopsPage
  },
  information: {
    pattern: '/informace',
    exactly: true,
    component: pages.InformationPage
  },
}