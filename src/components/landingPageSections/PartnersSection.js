import React from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Section, PartnerItem, Button } from '../'
import './PartnersSection.css'
import partner1 from '../../images/institut.png'
import partner2 from '../../images/nszm.png'
import partner3 from '../../images/ivs.png'
import partner4 from '../../images/noxmedia.png'

const partners = [
  {
    logo: partner1,
    type: 'Jsme političtí konzultanti',
    text: 'Analyzujeme, konzultujeme, školíme, vedeme kampaně. Kultivujeme a profesionalizujeme politický marketing v České republice. Sdružujeme profesionály z oboru.',
link : 'http://politickymarketing.com/',
  },
  {
    logo:  partner2,
    type: 'Národní síť Zdravých měst České republiky (NSZM ČR)',
    text: 'Národní síť Zdravých měst České republiky (NSZM ČR) je certifikovanou asociací měst, obcí a regionů, které jsou zapojeny do mezinárodního programu Zdravé město WHO. Zdravá města, obce a regiony systematicky podporují udržitelný rozvoj, kvalitu života a zdraví lidí prostřednictvím strategického řízení, aktivně se ptají svých obyvatel na jejich názory.',
    link: 'https://www.zdravamesta.cz/',
  },
  {
    logo:  partner3,
    type: 'Institut veřejné správy Ekonomicko-správní fakulty Masarykovy univerzity',
    text: 'IVS se podílí či sám realizuje řadu aktivit v oblastech výzkumu, vývoje či aplikace a to na pomezí mezi univerzitním prostředím a veřejnou správou. Mottem IVS je „spojovat a inovovat“. Subjekty zapojené do nejrůznějších aktivit a činností IVS pocházejí jak z univerzitního prostředí, tak oblasti veřejné správy, neziskového či soukromého sektoru.',
    link: 'http://ivs.econ.muni.cz/',
  },
  {
    logo:  partner4,
    type: 'NOX media | foto • video • audio',
    text: 'Fotíme, točíme a nahráváme. Zkátka tvoříme audiovizuální obsah. Od režírovaných reklam, sestřihů, přes fotoreportáže a reklamní fotografie až po reklamy do rádií.',
    link: 'http://noxmedia.cz/',
  },
]

const PartnersSection = () => (
  <Section title='Partneři & Sponzoři' type="white">
    <div className="partners-section">
      <div className="partners-section__partners">
        {partners.map(partner => (
          <PartnerItem
            logo={partner.logo}
            text={partner.text}
            link={partner.link}
            type={partner.type}
          />
        ))}
      </div>
      <div className="partners-section__text">
        {/* <Button text="Chci Vás podpořit" color="inverted" /> */}
        Chcete naše úsilí podpořit? Napište nám, rádi se s vámi setkáme osobně a probereme možnosti spolupráce.
      </div>
    </div>

  </Section>
)

export default (PartnersSection)