const objectToArray = (object) => {
  return Object.keys(object).map((i) => object[i])
}

export {
  objectToArray,
}