import React from 'react'
import PropTypes from 'prop-types'
import { Title, Button } from '../components'
import { Container, Row, Col } from '../components/grid'

import './NotFoundPage.css'

class NotFoundPage extends React.Component {
  componentDidMount() {
    document.title = "404 - stránka nenalezena | Kandiduju.cz"
  }
  render() {
    return (
      <div className="not-found-page">
        <Title message="Stránka nenalezena" />
        <div className="four-zero-four">404</div>
        <div className="not-found-text">{`Vámi hledaná stránka bohužel neexistuje. Vraťte se na úvodní stránku a zkuste své štěstí znovu.`}</div>
        <div>
          <Button text="Vrátit se zpět" link='/' />
        </div>
      </div>
    )
  }
}

NotFoundPage.propTypes = {
}

export default (NotFoundPage)
