import React from 'react'
import PropTypes from 'prop-types'
import './InfoItem.css'
import { Button } from './'

const InfoItem = ({ title, text, image, link }) => (
  <div className="info-item-wrapper">
    <div className="info-item">
      <div className="info-item__image">
        <img src={image} alt="" />
      </div>
      <div className="info-item__description">
        <div className="description">
          {text}
        </div>
        <div className="buttons">
          <Button text="Chci vědět víc" link={link} />
        </div>
      </div>
      <div className="info-item__title">
        <h2>{title}</h2>
      </div>
    </div>
  </div>
)

InfoItem.propTypes = {
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
}

export default InfoItem
