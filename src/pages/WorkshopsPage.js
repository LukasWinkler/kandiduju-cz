import React from 'react'
import { PageWrapper } from './'
import { WorkshopSignupForm } from '../components'
import { landImgs } from '../config'
import './WorkshopsPage.css'

class WorkshopsPage extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0)
    document.title = "Workshop | Kandiduju.cz"
  }
  render() {
    return (
     <PageWrapper title="Workshop" image={landImgs.land3}>
       <div className="workshops-page container">
         <div className="workshops-page__workshops">
           <div className="workshop">
             <div className="workshop__title">
               <h2>Velký letní workshop</h2>
             </div>
             <div className="workshop__date">
               10. - 12. 8. 2018
             </div>
             <div className="workshop__description">
               <p>
                 Chystáme pro Vás intenzivní víkendový workshop, kde se budeš moct setkat s kandidáty do zastupitelstev z celé republiky a kde budeš mít jedinečnou <b>možnost načerpat spoustu informací, motivaci a tipů do svého budoucího působení na radnici.</b> To vše od zkušených komunálních politiků a od organizací a akademiků zabývajících se fungováním měst a obcí. Na workshopu pokryjeme témata od financování komunitních projektů přes koncept chytrých měst po politický marketing. Více informací <b>již brzy</b> na těchto stránkách.
               </p>
             </div>
           </div>
           <div className="workshops-page__form">
             <WorkshopSignupForm />
           </div>
         </div>
       </div>
     </PageWrapper>
   )
  }
}

export default (WorkshopsPage)
