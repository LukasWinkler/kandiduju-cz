import React from 'react'
import { Form, Input, Button } from 'antd'
import firebaseConf from '../firebase'

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      password: '',
      email: '',
     }
  }
  handleChange = (e, field) => {
    const newObj = {}
    newObj[field] = e.target.value
    this.setState(newObj)
  }

  handleLogin = () => {
    const { password, email } = this.state
    firebaseConf.auth().signInWithEmailAndPassword(email, password).then((user) => {
      alert('You are logged in!')
      console.log('user', user)
    }).catch(error => console.log('An error has occured', error))
  }
  render() {
    const { handleChange } = this
    const { email, password } = this.state
    return (
      <Form style={style}>
        <Form.Item>
          <Input onChange={(e) => handleChange(e, 'email')} value={email}/>
        </Form.Item>
        <Form.Item>
          <Input onChange={(e) => handleChange(e, 'password')} value={password}/>
        </Form.Item>
        <Button onClick={this.handleLogin}>Login</Button>
      </Form>
    )
  }
}

const style = {
  display: 'flex',
  flexDirection: 'column',
  height: '50vh'
}

export default LoginPage