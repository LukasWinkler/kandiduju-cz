
import React from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import Slider from 'react-slick'
import { Section, Button } from '../'
import './HeaderSection.css'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const scrollTo = (id) => {
document.getElementById(id).scrollIntoView({behavior: "smooth"})
}


const settings = {
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
}


//className="header-section__background"
const HeaderSection = () => (
  <div className="header-section">
      <img src="https://res.cloudinary.com/farlanis/image/upload/v1525593588/land-a-min.png" className="header-section__background" alt="" />


    <div className="header-section__content">
      <div className="container header-container">
        <div className="first-row">
          <div className="first-block">
            <h1 className="motto-title">Kandiduju.cz</h1>
            <i className="motto-text">,,Chceš kandidovat? Ať jsi<br/>z Prahy nebo z Horní-Dolní, ukážeme Ti jak na to!’’</i>
          </div>
        </div>
        <div className="second-row">
          <div className="second-block">
            <h2>Co Ti můžeme nabídnout?</h2>
            <ul>
             <li>Spoustu relevantních informací na jednom místě</li>
             <li>Rozhovory s aktivními komunálními politky</li>
             <li>Velký letní workshop pro budoucí zastupitele</li>
            </ul>
          </div>
          <div className="second-row__buttons">
            <Button text="Chci vědět víc" link='/informace'/>
            <Button text="Kontaktovat" color="white" onClick={() => scrollTo('contact-us-section')} />
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default (HeaderSection)
