import React from 'react'
import PropTypes from 'prop-types'
import './ArticleType.css'

const types = {
  news: 'Aktualita',
  article: ' Článek',
  interview: 'Rozhovor',
}

const ArticleType = ({ type }) => (
  <div className={`article-type article-type--${type}`}>
    {types[type]}
  </div>
)

ArticleType.propTypes = {
  type: PropTypes.string.isRequired,
}

export default ArticleType
