// import React, { Component } from 'react';
// import logo from './logo.svg';
// import './App.css';

// class App extends Component {
//   render() {
//     return (
//       <div className="App">
//         <header className="App-header">
//           <img src={logo} className="App-logo" alt="logo" />
//           <h1 className="App-title">Welcome to React</h1>
//           <h1 className = "App-title" > Welcome to React </h1>
//         </header>
//         <p className="App-intro">
//           To get started, edit <code>src/App.js</code> and save to reload.
//         </p>
//       </div>
//     );
//   }
// }

// export default App;

import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import debounce from 'debounce';
import { SwitchWithRoutes } from './components/routing';
//import { Footer } from '../components'
import { CookiesInfo, Footer, Header } from './components';

import meta from './app/meta';

import './app/App.css';

// routes
import routes from './routes';

//eslint-disable-next-line
class App extends React.Component {
	componentDidCatch(error, info) {
		console.log(info);
		console.log(error);
	}
	render() {
		return (
			<div className="app-container">
				<Helmet
					htmlAttributes={{
						lang: 'cs'
					}}
					titleTemplate={meta.title}
					meta={meta.metas}
					link={meta.links}
				/>
				<Header
					// close={closeNavigation}
					// toggle={toggleNavigation}
					// visible={navigationVisible}
					// scrolled={navigationOverlayVisible}
				/>
				<SwitchWithRoutes routes={routes}/>
				<CookiesInfo />
				<Footer />
			</div>
		);
	}
}

App.propTypes = {
	// errors: PropTypes.object.isRequired,
	// navigationVisible: PropTypes.bool.isRequired,
	// toggleNavigation: PropTypes.func.isRequired,
	// closeNavigation: PropTypes.func.isRequired,
	// setScrollLocation: PropTypes.func.isRequired,
	// setCurrentLocale: PropTypes.func.isRequired,
	// navigationOverlayVisible: PropTypes.bool.isRequired,
	// start: PropTypes.func.isRequired
};

export default (App);
