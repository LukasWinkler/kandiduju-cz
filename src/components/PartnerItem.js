import React from 'react'
import PropTypes from 'prop-types'
import './PartnerItem.css'

const PartnerItem = ({ logo, type, text, link }) => (
  <div className="partner-item-wrapper">
    <div className="partner-item">
      <div className="partner-item__image">
        <a href={link} target="_blank">
          <img src={logo} alt="" />
        </a>
      </div>
      <div className="partner-item__description">
        <div className="type">
          <div className="type__title">{type}</div>
        </div>
        <div className="description">
          {text}
        </div>
      </div>
    </div>
  </div>
)

PartnerItem.propTypes = {
  type: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  logo: PropTypes.string.isRequired,
}

export default PartnerItem
