import React from 'react'
import PropTypes from 'prop-types'

const Container = ({ fluid, children, className }) =>
  <div className={`${className} ${fluid ? 'container-fluid' : 'container'}`}>
    {children}
  </div>

Container.defaultProps = {
  className: '',
  children: null,
  fluid: false
}

Container.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any,
  fluid: PropTypes.bool,
}

export default Container
