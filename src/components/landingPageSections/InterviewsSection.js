import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import Slider from 'react-slick'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { Section, Button, ArticleItem } from '../'
import './InterviewsSection.css'


const Interview = ({ interview }) => (
  <div className="interview">
    <div className="interview__image">
      <img src={interview.image.url} />
    </div>
    <div className="interview__name">
      {interview.name}
    </div>
    <div className="interview__buttons">
      <Button text="Přejít na rozhovor" link={`/clanky/${interview.slug}`}/>
    </div>
  </div>
)

class InterviewsSection extends React.Component {
  render() {
    const {articles} = this.props
    const interviews = Object.values(articles).filter(article => article.type === 'interview').slice(0, 3)
    return (
      <Section title='Nejnovější rozhovory' type="grey" >
        <div className="interviews-section">
          <div className="interviews-section__interviews">
            <Slider
              dots
              draggable
              infinite
              autoplay
              arrows={false}
              autoplaySpeed={5000}
            >
              {interviews.map(interview => <Interview interview={interview} />) }
            </Slider>
          </div>
        </div>
      </Section>
    )
  }
}

export default (InterviewsSection)
