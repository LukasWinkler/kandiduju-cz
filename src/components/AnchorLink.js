import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

class AnchorLink extends React.PureComponent {
  render() {
    const { to, children, className, onClick, isInline } = this.props
    const style = {}
    if (isInline) style.display = 'inline'
    const splited = to.split('#')
    const hash = (splited.length > 1) ? splited[splited.length - 1] : ''
    if (!children) {
      return (
        <div />
      )
    }
    if (hash) {
      return (
        <div className={className} style={style} onClick={() => { onClick && onClick(); }}>
          <Link to={to}>
            {children}
          </Link>
        </div>
      )
    }
    return (
      <div onClick={() => { onClick && onClick(); }} style={style} className={className}>
        <Link to={to}>{children}</Link>
      </div>
    )
  }
}

AnchorLink.propTypes = {
  children: PropTypes.any,
  to: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  className: PropTypes.string,
  isInline: PropTypes.string,
}

AnchorLink.defaultProps = {
  isInline: false,
}

export default AnchorLink
